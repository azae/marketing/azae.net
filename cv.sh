#!/bin/bash
set -e

API_BASE="https://gitlab.com/api/v4"
CV_PROJECT_ID=5392399
CV_PROJECT_URL="$API_BASE/projects/$CV_PROJECT_ID"

[ -n "${1:-}" ] && TOKEN="${1}" 

if [[ -z "$TOKEN" ]]; then
   echo 'No $TOKEN provided'
   exit 1
fi

# Jobs functions
function curl_() {
  curl --silent --header "PRIVATE-TOKEN: $TOKEN" $@
}
function list_jobs() {
  curl_ "$CV_PROJECT_URL/jobs?scope=success"
}

function jq_() {
  jq --raw-output --compact-output "$@"
}
function reformat_json() {
  jq_ '.[] | {"id": .id, "branch": .ref  }'
}

function get_last_job() {
  grep master | head -n 1
}
function reformat_id_only() {
  jq_ '. | .id'
}
function get_last_job_id() {
  list_jobs | reformat_json | get_last_job | reformat_id_only
}

function download_cv() {
  PATH_ARTIFACT=$1
  DESTINATION=$2
  FILE_URL="$CV_PROJECT_URL/jobs/$LAST_JOB_ID/artifacts/$PATH_ARTIFACT"
  echo "Dowload from \"$FILE_URL\""
  curl_ $FILE_URL > $DESTINATION
  echo "cv downloaded from $PATH_ARTIFACT to $DESTINATION"
}
function update_cvs() {
  LAST_JOB_ID=`get_last_job_id`
  echo "Last job id is $LAST_JOB_ID"
  download_cv alexis.benoist/alexis.benoist.pdf content/team/benoist_alexis/cv.pdf
  download_cv julie.quille/julie.quille.pdf content/team/quille_julie/cv.pdf
  download_cv thomas.clavier/thomas.clavier.pdf content/team/clavier_thomas/cv.pdf

  download_cv olivier.albiez/olivier.albiez.pdf content/team/albiez_olivier/cv.pdf
}
update_cvs
git status
