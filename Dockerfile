FROM registry.gitlab.com/tclavier/docker-nginx

RUN apt-get update \
 && apt-get install -y \
    imagemagick \
    make \
    git \
    wget \
 && apt-get clean

ENV HUGO_VERSION=0.129.0
RUN wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_linux-amd64.deb -O /tmp/hugo.deb \
 && dpkg -i /tmp/hugo.deb \
 && rm -f /tmp/hugo.deb

COPY . /site
WORKDIR /site

RUN HUGO_ENV=production hugo --destination=/var/www/prod
COPY nginx_vhost.conf /etc/nginx/conf.d/azae.conf

RUN hugo --buildDrafts --buildFuture --destination=/var/www/draft \
 && echo "User-agent: *" > /var/www/draft/robots.txt \
 && echo "Disallow:/ "  >> /var/www/draft/robots.txt
