---
layout: single
title: Charte déontologique de coaching
permalink: /charte/
description: |
---


Code de déontologie
de la FNP



PREAMBULE

Tous les Instituts affiliés à la FNP, ainsi que les membres individuels, sont tenus d'exercer leur profession avec un sens particulièrement aigu de leurs responsabilités vis-à-vis de leur propre personne, de leur travail thérapeutique et des personnes avec lesquelles une relation particulière est créée par le biais du traitement psychothérapeutique et/ou psychanalytique. Les Instituts affiliés à la FNP sont dans l'obligation de prêter une attention toute particulière aux questions de déontologie. Cela s'applique aux formateurs, aux membres et aux inscrits des Instituts affiliés à la FNP.

Ces règles de déontologie :

- visent à protéger le patient/analysant contre les applications abusives de la psychanalyse et/ou de la psychothérapie par les praticiens ou les formateurs,
- servent de règles de conduite à leurs membres,
- servent de référence en cas de plainte ou de litige.



2 - LA PROFESSION DE PSYCHANALYSTE et de PSYCHOPRATICIEN ANALYTIQUE

La profession de psychanalyste et de psychopraticien analytique est une discipline spécifique du domaine des sciences humaines. Elle implique un diagnostic et une stratégie globale et explicite de traitement des troubles psychologiques, sociaux et psychosomatiques. Les méthodes utilisées reposent sur des théories scientifiques et les concepts de la métapsychologie et de la psychothérapie.

Par le biais d'une interaction entre un patients/analysants et un psychanalyste ou un psychopraticien analytique, ce traitement a pour objectif de déclencher un processus thérapeutique permettant des changements et une évolution à long terme.

La profession de psychanalyste et de psychopraticien analytique se caractérise par l'implication du thérapeute dans la réalisation des thérapies précitées.

Le psychanalyste et le psychopraticien analytique sont tenus d'utiliser leurs compétences dans le respect des valeurs et de la dignité de leurs patients/analysants au mieux des intérêts de ces derniers.

Le psychanalyste et le psychopraticien analytique doivent indiquer leurs niveaux de qualification dans la spécialité où ils ont été formés.

3 - COMPETENCE PROFESSIONNELLE ET PERFECTIONNEMENT

## Compétences

Nos coaches se tiennent régulièrement informés des avancées dans le coaching et les outils associés.

## Secret professionnel

Nos coachs Le psychanalyste et le psychopraticien analytique et leur équipe éventuelle sont soumis au secret professionnel absolu concernant tout ce qui leur est confié dans l'exercice de leur profession. Cette même obligation s'applique dans le cadre de la supervision.

## Clarté du cadre

Au début de l'accompagnement,

5 - CADRE DE LA THERAPIE

Dès le début de la thérapie, le psychanalyste et le psychopraticien analytique doivent attirer l'attention de leur patient sur ses droits et souligner les points suivants :

- type de méthode employé (psychothérapie ou psychanalyse). Il précise le cadre de ce travail (y compris les conditions d'annulation ou d'arrêt),

- durée de chaque séance, qui doit être au minimum de 45 minutes,

- conditions financières (honoraires, prises en charge, règlement des séances manquées),

- secret professionnel absolu, même vis-à-vis de son propre superviseur (anonymat des patients),

- règle de l’absence de jugement : neutralité bienveillante,

- règle de l’interdit du toucher (poignée de mains pour se saluer uniquement),

- règle du libre choix du thérapeute : le patient doit pouvoir décider lui-même si et avec qui il veut entreprendre un traitement, et être libre de l’interrompre à tout moment.

Le psychanalyste et le psychopraticien analytique sont dans l'obligation d'assumer leurs responsabilités compte tenu des conditions particulières de confiance et de dépendance qui caractérisent la relation thérapeutique. Il y a abus de cette relation à partir du moment où le psychanalyste ou le psychopraticien analytique manque à son devoir et à sa responsabilité envers son patient pour satisfaire son intérêt personnel (par exemple, sur le plan sexuel, émotionnel, social ou économique). Toute forme d'abus représente une infraction aux directives déontologiques spécifiques concernant la profession de psychanalyste et de psychopraticien analytique. L'entière responsabilité des abus incombe au praticien. Tout agissement irresponsable dans le cadre de la relation de confiance et de dépendance créée par la thérapie constitue une grave faute professionnelle.

6 - OBLIGATION DE FOURNIR DES INFORMATIONS EXACTES ET OBJECTIVES

Les informations fournies au patient concernant les conditions dans lesquelles se déroule le traitement doivent être exactes, objectives et reposer sur des faits.

Toute publicité mensongère est interdite. Exemples :

- promesses irréalistes de guérison,

- référence à de nombreuses approches thérapeutiques différentes, ce qui laisserait supposer une formation plus étendue qu'elle ne l'est en réalité (formations entamées et non terminées).

7 - RELATIONS PROFESSIONNELLES AVEC LES COLLEGUES

Si nécessaire, le psychanalyste et le psychopraticien analytique doivent travailler de manière interdisciplinaire avec des représentants d'autres sciences, dans l'intérêt du patient.

8 - PRINCIPES DEONTOLOGIQUES CONCERNANT LA FORMATION

Ces principes déontologiques s'appliquent également, par analogie, aux rapports entre formateurs et élèves.

9 - CONTRIBUTION A LA SANTE PUBLIQUE

La responsabilité des psychanalystes et des psychopraticiens analytiques au niveau de la société exige qu'ils travaillent à contribuer au maintien et à l'établissement de conditions de vie susceptibles de promouvoir, sauvegarder et rétablir la santé psychique, la maturation et l'épanouissement de l'être humain.

10 - RECHERCHE EN PSYCHANALYSE ET PSYCHOTHERAPIE

Afin de promouvoir l'évolution scientifique de la psychanalyse et de la psychothérapie analytique et l'étude de leurs effets, le psychanalyste et le psychopraticien analytique doivent, dans la mesure du possible, collaborer à des travaux de recherche entrepris dans ce sens.

Les principes déontologiques définis plus haut doivent également être respectés à l'occasion de ces travaux de recherche et lors de leur publication. Les intérêts du patient restent prioritaires.

11 - INFRACTIONS AUX REGLES DE DEONTOLOGIE

La FNP permet le recours auprès de son Comité d’Ethique pour arbitrer les éventuels litiges.

12 - OBLIGATIONS DES INSTITUTS DE FORMATION MEMBRES DE LA FNP

Ces instituts de formation doivent exiger que leurs membres formateurs et praticiens respectent les règles du code de déontologie de la FNP.
