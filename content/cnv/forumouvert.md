---
title: Forum Ouvert CNV
permalink: /forumouvert/
description: |
    Co-apprentissage, transmission, vivre la CNV dans le fond mais aussi dans la forme c'est ce que nous vous proposons samedi 1er mai 2021 de 13h30 à 18h.

---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
<div class="col-md-12">
<div class="proposition">
 <h2>La proposition</h2>
 <div class="girafe">
  <ul>
   <li>4h30 de CNV,</li>
   <li>Co-apprentissage,</li>
   <li>Transmission,</li>
   <li>Vivre la CNV dans le fond (sujet principal) mais aussi dans la forme (autonomie, liberté, responsabilité, demande)</li>
  </ul>
 </div>
</div>
<hr>
<ul class="participation">
 <li><a class="bouton sinscrire" href='https://www.weezevent.com/forum-ouvert-cnv-samedi-1-mai-2021'>S'INSCRIRE</a></li>
 <li><a class="bouton sabonner" href='http://eepurl.com/hnf_9j'>S'ABONNER</a></li>
</ul>
<ul class="infospratiques">
 <li>Thème</li>
 <li>Prendre soin de soi... mais pas n'importe comment !</li>
 <li><em>Samedi 01 mai 2021</em></li>
 <li>13h30 à 18h en ligne</li>
</ul>
<hr>
<p>
<strong>L'essentiel</strong>
<ul>
 <li><a href='https://www.weezevent.com/forum-ouvert-cnv-samedi-20-mars-2021'>Inscription Forum Ouvert CNV</a></li>
 <li><a href='http://eepurl.com/hnf_9j'>Formulaire pour être tenu·e informé·e des prochains</a></li>
 </ul>
</p>
<h2 class='color-orange'>Quelques idées de ce qui pourrait se trouver dans le thème</h2>
<p>
    Voici des idées de ce que nous pourrions découvrir à l'intérieur de ce thème, et peut-être découvrirons-nous des choses totalement différentes. La magie du Forum Ouvert c'est que ce sont les personnes présentes qui créent le programme.
<ul>
 <li>Comment prenons nous soin de nos ressources ?</li>
 <li>Comment verifions-nous que nous sommes en ÉTÉ ?</li>
 <li>Comment éviter de faire de la CNV une exigence de plus envers soi et les autres (notamment pour celleux qui débutent) ?</li>
 <li>Transmetteur•rice•s : ne pas faire peser la responsabilité de nourrir nos besoins de reconnaissance et d'amour sur les participant•e•s.</li>
 <li>empathie, supervision, investissement perso : de qui prenons nous soi et qui prend soin de nous ?</li>
</ul>
</p>
<h2 class='color-sky-blue'>Le forum ouvert, qu'est-ce que c'est ?</h2>
<p>
Un espace où l'on va vous proposer d'être <strong>acteur·rice de ce que vous souhaitez vivre</strong> durant 4h entières.... du coup, comme vous pouvez l'imaginer il est difficile de vous dire dès maintenant de quoi chacun·e des participant·e·s aura besoin le jour même... mais nous allons l'expérimenter !
</p>
<p>
Néanmoins, il est possible que certain·e d'entre nous ait envie de <strong>demander de l'aide</strong> sur une notion, un concept, un exercice, d'autre envie de tester un atelier ou une présentation et <strong>avoir du feedback</strong>, ou encore l'envie de parler de tout autre chose, ou peut-être pas, nous ne le saurons que le moment venu.
</p>
<p>
Pour en savoir plus :
<a href='https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert'>Forum Ouvert</a>
</p>
<h2 class='color-chameleon'>D'où vient cette idée d'un forum ouvert CNV ?</h2>
<p>
Cette idée germe depuis un moment dans ma tête où je nourris la croyance que nous avons toutes et tous des trésors à partager, et aussi des endroits de difficultés spécifiques à chaque individu.
</p>
<p>
Ainsi je souhaite mettre en place un format :
<ul>
 <li>qui s'adapte,</li>
 <li>qui soutienne l'idée du lien avant le résultat,</li>
 <li>qui privilégie la liberté, la responsabilité et l'autonomie,</li>
 <li>qui soit aussi accessible que possible au niveau du prix...</li>
</ul>
... et c'est évidemment le <strong>forum ouvert</strong> qui m'est apparu comme une évidence !
</p>
<p>
Raphaël Pierquin avec qui j'ai déjà eu la chance de faire des forum ouvert sera encore des notres pour cette deuxième édition.
</p>
<h2 class='color-orange'>À qui s'adresse cet événement ?</h2>
<p>
L'événement est encore en construction, le groupe se cherche et apprend à se connaitre, ainsi il ne me semble pas que ce soit le lieu idéal pour découvrir la CNV. Mais si vous avez l'esprit d'un·e aventurier·ière vous êtes bienvenue !!
     Pour cette deuxième édition il est également demandé à chacun·e de <strong>prendre en main l'outil Gather Town avant l'événement</strong>.
    </p>
    <p>
     D'ailleurs si vous avez des personnes autour de vous qui vous paraissent correspondre à ces critères n'hésitez pas à passer l'info (tout en gardant en tête qu'il n'y a qu'une vingtaine de places).
    </p>
    <h2 class='color-scarlet-red'>Bon d'accord d'accord, moi je suis dispo, j'ai envie, j'ai tout compris (ou pas d'ailleurs) mais je veux participer : comment je fais ?</h2>
    <p>
     Facile !! Il suffit de <strong>s'inscrire sur ce lien</strong> !
    </p>
    <p>
     <a href='https://www.weezevent.com/forum-ouvert-cnv-samedi-20-mars-2021'>Inscription Forum Ouvert CNV</a>
    </p>
    <p>
     Et comme il y a relativement peu de place, ne vous inscrivez que si vous êtes certain·e de venir ;-)
    </p>
    <p>
     Voilà vous savez tout !!! À très bientôt peut-être :-D
    </p>
    <h2 class='color-chameleon'>Petit historique des Forum Ouvert CNV</h2>
    <p>  
      <h4 class='color-chameleon'>Forum Ouvert et rétrospectives :</h4>
      <p>
      <strong>Ce que nous retenons de chaque itération et les expérimentations que nous faisons pour la suivante</strong> (merci à Raphaël de rappeler le principe des petits pas et de ne pas faire 200 changements à la fois) :
      </p>
      <ul>
       <li>
          <strong>Samedi 20 mars - Forum Ouvert CNV : La violence dans la CNV</strong>
          <ul>
             <li>Envie de se connaitre un peu plus avant de se lancer : se connecter 30 min plus tôt pour faire un tour de connexion / inclusion : besoin de lien, de connexion et de sécurité.</li>
            </ul>
       </li>
       <li>
          <strong>Samedi 6 février - Forum Ouvert CNV : Liberté et responsabilité dans mon apprentissage</strong>
            <ul>
             <li>Beaucoup de difficultés avec les outils : chercher / fabriquer des tutos pour que chacun·e soit autonome sur Gather Town et Miro avant l'événement : besoin de simplicité et d'efficacité.</li>
             <li>Quelques difficultés à mener des sessions (échanges nourrissants / apprentissages / prise de notes) : proposer des modèles de déroulé pour que les personnes qui le souhaitent puissent avoir un soutien : besoin de soutien et de structure.</li>
             <li>Questionnement sur la précision de la thématique : choix de créer des espaces de co-construction de la thématique : besoin de clarté et de co-responsabilité.</li>
            </ul>
       </li>
      </ul>
    </p>
