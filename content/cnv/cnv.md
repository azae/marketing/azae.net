---
title: Informations globales
permalink: /cnv/
description: |
    Nos actions autour de la CNV

---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
  <div class="col-md-12">
    <div style="text-align:center"> 
       <h2 class='color-sky-blue'>Découvrir et approfondir la CNV avec des ateliers formats courts et moyens</h2>
    </div>
    <p>
     <table>
      <tr>
       <td>
       <div style="text-align:center">
       <strong>Initiation à la Communication NonViolente</strong>
       </div>
       </td>
       <td>
       <div style="text-align:center">
       <strong>Approfondissement CNV</strong>
       </div>
       </td>
       <td>
       <div style="text-align:center">
       <strong>Forum Ouvert CNV</strong>
       </div>
       </td>
      </tr>
      <tr>
       <td>
       <a href='https://azae.net/cnv/initiation/'><img alt='image girafe' class='pull-left' src='/assets/images/giraffe1.png' height="100"></a>
       </td>
       <td>
       <a href='https://azae.net/cnv/approfondissement/'><img alt='image girafe' class='pull-left' src='/assets/images/giraffe2.png' height="100"></a>
       </td>
       <td>
       <a href='https://azae.net/cnv/forumouvert/'><img alt='image girafe' class='pull-left' src='/assets/images/forum-ouvert.jpg' height="100"></a>
       </td>
      </tr>
      <tr>
       <td>
       <p>
       Atelier de 90 min qui permet d'aborder les bases de la CNV : Philosophie et pratique.
       </p>
       <p>
       Sans pré-requis - 15 personnes maximum
       </p>
       </td>
       <td>
       <p>
       Atelier de 4 heures au cours duquel nous approfondissons une thématique particulière de la CNV. 
       </p>
       <p>
       Pré-requis : être familier·e des bases - 15 pers. maximum
       </p>
       </td>
       <td>
       <p>
       Demie-journée d'échange et de pratique sous le format du Forum Ouvert
       </p>
       <p>
       Pré-requis : avoir une vingtaine de journée de formation
       </p>
       </td>
      </tr>
      <tr>
       <td>
       <p>
       Tarif : don libre (pas de recommandation)
       </p>
       <p>
       <a href='https://azae.net/cnv/initiation/'>En savoir plus</a></li>
       </p>
       </td>
       <td>
       <p>
       Tarif : don libre (recommandation 50€)
       </p>
       <p>
       <a href='https://azae.net/cnv/approfondissement/'>En savoir plus</a></li>
       </p>
       </td>
       <td>
       <p>
       Tarif : nous contacter
       </p>
       <p>
       <a href='https://azae.net/cnv/forumouvert/'>En savoir plus</a></li>
       </p>
       </td>
      </tr>
     </table>
    </p>
<hr>
<div style="text-align: center">
 <h3 class='color-chameleon'><strong>Vous souhaitez soutenir la démarche, c'est ici</strong></h3>
 <a class="plc" href='https://lydia-app.com/collect/71818-cnv-julie-diane/fr'>PARTICIPATION CONSCIENTE ET SOUTIEN</a>
 </div>
