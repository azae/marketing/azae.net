---
title: Découverte de la Communication NonViolente
permalink: /initiation/
description: |
    Découverte de la Communication NonViolente : philosophie et pratique.

---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
<div class="col-md-12">
<header class="proposition">
 <p>La proposition</p>
 <div class="girafe">
  <ul>
   <li>Découverte CNV</li>
   <li>90 min</li>
   <li>un groupe de 4 à 15 personnes passionnantes</li>
   <li>2 animatrices passionnées</li>
   <li>de l'échange, du lien, de l'expérimentation et des apprentissages</li>
  </ul>
 </div>
</header>
<hr>
<ul class="participation">
 <li><a class="bouton sinscrire" href='https://www.weezevent.com/decouverte-communication-nonviolente'>S'INSCRIRE</a></li>
 <li><a class="bouton sabonner" href='https://5c5fda8f.sibforms.com/serve/MUIEAHHFl0Svw8_xI01j4j2hkRai6otFy9odx4CIx4ef2oJwauaDteoH4vVBpbe-BQy8uBnD6VqaVLjTJasb94qxqyHrE8vDTcKGa_D2-OyCIpG5tfgSPPUskjp6xtOs4k384_jd1QptB-gFaWWfN9oAqNNOkpOM7-lxx7jEPVUz3f1yDFSAMqzBlzn5o_KKE_zX3Dh5jZxD8aeZ'>S'ABONNER</a></li>
 <li><a class="bouton soutenir" href='https://lydia-app.com/collect/71818-cnv-julie-diane/fr'>SOUTENIR</a></li>
</ul>
<ul class="infospratiques">
 <li><em>Vendredi 9 avril</em></li>
 <li><em>Dimanche 9 mai</em></li>
 <li>14h à 15h30 en ligne</li>
</ul>
<hr>
<div>
 <p>Autres liens</p>
 <ul>
  <li><a href='https://mobilizon.azae.net/@cnv_un_peu_beaucoup_passionnement_a_la_folie_'>Inscription sur Mobilizon</a></li>
  <li><a href='https://azae.net/cnv/approfondissement/'>Pour aller plus loin : Ateliers d'exploration</a></li>
 </ul>
</div>
<hr>
<h2 class='color-orange'>La découverte de la Communication NonViolente, qu'est-ce que c'est ?</h2>
<p>
Un espace participatif où nous nous proposons de vous partager la Communication NonViolente à travers notre vécu, nos expériences et notre vision du monde.
</p>
<p>
Pour nous, avant d'être un outil, une méthode, la Communication NonViolente est une philosophie de vie qui nous invite à croire en la coopération et l'abondance.
<a href='https://cnvfrance.fr/communication-non-violente/'>Vous pouvez consulter ici la définition de l'Association Française de la CNV</a>
</p>
<h2 class='color-chameleon'>Pourquoi ces temps "découverte" ?</h2>
<p>
La découverte de la CNV a changé nos vies et depuis nous y dédions une grande partie de notre temps. 
</p>
<p>
Nous transmettons la CNV autour de nous, parmis nos familles, nos ami·e·s mais également en entreprise et dans le milieu carcéral. Frustrées de voir que les formations à la CNV ne touchent encore qu'une toute petite partie d'une population globalement privilégiée nous cherchons, à notre niveau, des stratégies pour toucher de nouvelles personnes avec :
<ul>
 <li>un format court sans demande de pré-requis</li>
 <li>une participation financière libre</li>
 <li>une démarche de création de contenu pédagogique favorisant l'accessibilité</li>
</ul>
</p>
<h2 class='color-scarlet-red'>Notre couleur particulière</h2>
<h3 class='color-orange'>Qui sommes nous ?</h3>
<p>
Nous sommes deux femmes passionnées des relations interpersonnelles avec le doux rêve de changer le monde ! Dans la mouvance des différents mouvements de NonViolence, nous cherchons à incarner le changement que nous souhaitons voir dans le monde. 
</p>
<p>
Fières du chemin que nous avons parcouru depuis notre découverte de la CNV, nous essayons de transmettre cette manière de voir le monde, avec tout ce que nous sommes : nos forces, nos failles, notre système de croyance... Nous parions sur le fait qu'ensemble nous sommes plus fort·e·s, c'est donc AVEC les personnes qui viennent que nous créons un instant présent différent à chaque fois.
    </p>
   <div style="text-align:right">
     <strong>Diane Martin</strong> & <strong>Julie Quillé</strong>
    </div>
     <h3 class='color-orange'>Particularités de notre transmission</h3>
    <p>
     Nous croyons en l'apprentissage par l'expérience ! Nous essayons donc, dès l'initiation, de vivre la CNV : c'est à dire <strong>partager ce qui est vivant</strong>, et <strong>chercher ensemble comment nous rendre la vie plus belle</strong>.
    </p>
    <p>
     Cette façon de faire un peu originale par rapport à ce dont nous avons l'habitude peut être : curieuse, dérangeante, joyeuse, inconfortable, source de confusion selon chacun·e.
    </p>
     <h3 class='color-orange'>Ce que nous ne faisons pas</h3>
    <p>
     <ul>
       <li>Nous ne vous transmettrons aucune certitude, aucune vérité absolue.</li>
     </ul>
     Par ailleurs, nous ne sommes pas formatrices certifiées en CNV, vous pourrez trouver des <a href='https://www.cnvformations.fr/'>formations délivrées par des formateur·rice·s certifié·e·s ici</a> et pouvons également vous aider à trouver des formations qui vous conviennnent si besoin.
    </p>
    <h2 class='color-sky-blue'>Tarif et soutien</h2>
    <p>
    Nous avons à coeur de rendre la CNV accessible au plus grand nombre, ainsi nous ne souhaitons pas rajouter aux contraintes horaires et techniques des contraintes financières. C'est pourquoi il ne vous est pas demandé de prix fixe pour vous inscrire.
    </p>
    <p>
    Cependant nous investissons une grande partie de notre temps et de notre énergie pour pouvoir proposer ces espaces. Ainsi nous vous invitons à un prix libre, en fonction de ce qui est ajusté pour vous en prenant en compte vos ressources, ce que l'initiation vous a apporté et comment vous vous sentez.
    </p>
    <a href='http://www.bouddhisme-action.net/la-participation-consciente/'>Quelques informations sur la Participation Consciente</a>
<hr>
<div style="text-align: center">
 <h3 class='color-chameleon'><strong>Vous souhaitez soutenir la démarche, c'est ici</strong></h3>
 <a class="bouton plc" href='https://lydia-app.com/collect/71818-cnv-julie-diane/fr'>PARTICIPATION CONSCIENTE ET SOUTIEN</a>
 </div>
