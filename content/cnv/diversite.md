---
title: Accompagnement neuro-atypie
permalink: /diversite/
description: |
    Nos actions autour de la neurodiversité

---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
  <div class="col-md-12">
  <div style="text-align:center">
    <h2 class='color-sky-blue'>De la sensibilisation à l'accompagnement sur le terrain, nous vous accompagnons dans toutes les démarches pour l'inclusion des personnes neuro-atypiques</h2>
  </div>
  <div>
     <h3 class='color-sky-blue'><strong>Partenariat Talens d'As & Azaé</strong></h3>
  </div>
  <a href='https://azae.net/cnv/approfondissement/'><img alt='Logo Talent D'as' class='pull-left' src='/assets/images/talentsdas.jpeg' height="45"></a>
  <div style="text-align:center">
    C’est dans l’union de leurs compétences transverses que Clémence, psychologue clinicienne, et Julie, coache, accompagnent aujourd’hui les personnes et les structures. Elles-mêmes concernées ou en lien étroit avec la neurodiversité et en particulier le TSA (Trouble du Spectre Autistique) et le TDA (Trouble du Déficit de l'Attention). Elles partagent leurs expériences de l’accompagnement autant au niveau des outils que de l’organisation, pour soutenir les équipes dans leur réussite à créer des expériences d’inclusions positives tant pour les personnes neuro-atypiques que pour leurs collègues.
  </div>
  <hr>
  <div>
   <h3 class='color-sky-blue'><strong>Domaines d'expertise</strong></h3>
  <p>
  Notre objectif est de soutenir l'intégration des personnes neuro-atypiques (HP, TSA, TDA/H, dys etc.) en accompagnant les équipes et les individus à chaque étape du process. Ainsi nos interventions s'orientent essentiellement autour de 3 axes :
  <ol>
    <li><h4 class='color-sky-blue'>formation</h4>
      <ul>
        <li>sensibilisation et approfondissement sur les spécificités des personnes neuro-atypiques</li>
        <li>formation aux outils et méthodologies en lien avec l'accompagnement des personnes neuro-atypiques et l'animation de groupe socio-professionnels</li>
        <li>formation en lien avec l'organisation d'équipe (en particulier dans les milieux associatifs et PME à organisation innovantes)</li>
      </ul>
      Organisme de formation certifié Qualiopi, nos formations sont éligibles aux financements OPCO.
    </li>
    <li><h4 class='color-sky-blue'>conseil - facilitation - animation</h4>
    <ul>
      <li>accompagnement spécifique sur l'atteinte d'objectifs concrets par exemple :
          <ul>
           <li>définition des profils accompagnés,</li>
           <li>check-list des comportements socio-professionnels à acquérir,</li>
           <li>adaptation de poste,</li>
           <li>outils d'auto-évaluation de la personne neuro-atypique pendant l'emploi,</li>
           <li>définition de process etc.</li>
         </ul>
      </li>
      <li>Co-construction des groupes d'habiletés socio-professionnelles</li>
      <li>Facilitation de groupes</li>
      </ul>
  </li>
  <li><h4 class='color-sky-blue'>coaching</h4>
     <ul>
      <li>accompagnement de terrain des personnes neuro-atypiques (entreprise, famille, réunion équipe)</li>
      <li>job-coaching pour l'intégration de personne neuro-atypiques en milieu professionnel</li>
      <li>coaching / supervision et accompagnement individuel et/ou collectif sur leur pratique et leurs objectifs</li>
     </ul>
  </li>
  </ol>
  </p>
   </div>
   <hr>
  <div style="text-align:center">
       <h2 class='color-sky-blue'><strong>Qui sommes-nous?</strong></h2>
    </div>
    <p>
       <a href='https://azae.net/cnv/initiation/'><img alt='Clémence Petit' class='pull-left' src='/assets/images/clemencepetit.jpeg' height="180"></a>
       <div class='color-sky-blue'>
       <p><strong>Clémence Petit</strong> est psychologue clinicienne, spécialisée dans l’accompagnement des adolescents et adultes autistes et Asperger. Elle est diplômée d’un Master de psychologie clinique, spécialisation troubles du spectre autistique de l’Université Paris-Descartes qu’elle a complété par une formation au Job-Coaching auprès de la National Autistic Society de Londres puis par des formations auprès du Dr Isabelle HENAULT à Montréal.
       </p>
       <p>
       Clémence exerce en continu depuis 10 ans le coaching individuel d’adultes Aspergers, ainsi que dans diverses entreprises qui emploient des adultes autistes et Asperger. Elle propose également des groupes de socialisation et des groupes d’apprentissage des habiletés socio-professionnelles adaptés aux adolescents et adultes TSA. En plus de cette expérience professionnelle, Clémence est précédemment co-fondatrice de diverses associations spécialisées dans l’accueil, le soutien et l’accompagnement des adultes Asperger. Elle y a exercé, depuis leurs créations en 2008, les fonctions de psychologue référente ainsi que d’organisatrice et d’animatrice des ateliers collectifs de socialisation.
       </p>
       </div>
       <a href='https://azae.net/cnv/approfondissement/'><img alt='Julie Quillé' class='pull-left' src='/assets/images/juliequille.jpeg' height="180"></a>
       <div class='color-sky-blue'>
       <p><strong>Julie Quillé</strong> est coache, facilitatrice, formatrice spécialisée dans les dynamiques de groupes et l’inclusion. Passionnée par l’intelligence collective et le travail en groupe elle intervient autant au niveau des équipes que des individus pour favoriser le « faire ensemble » en prenant en compte les spécificités de chaque individu.
       </p>
       <p>
       Ayant été longtemps investie dans le milieu associatif, notamment à Mensa où elle a pu soutenir les membres dans leur découverte de leur neuro-atypie, elle intervient aujourd’hui dans diverses structures allant de l’associatif à la grande entreprise, en passant par le milieu carcéral. Gestion de conflit, méthodologie de travail, groupe de discussions sont autant de thématiques qu’elle aborde que ce soit par ses compétences en facilitant directement ou pour accompagner les individus et les structures à acquérir ces compétences.
       </p>
       <p>
       Une partie de son travail est ciblé sur l’inclusion de la diversité au travail. Ainsi elle aborde les thématiques liées au genre, au racisme, et à la neuro-diversité.
       </p>
       </div>
    </p>


<hr>
