---
nositemap: true
name: Somé Thibaut
id: tsome
vcard_name: "Somé;Thibaut"
email: thibaut.some@azae.net
mobile: ""
organisations:

social:
  - url: https://www.linkedin.com/in/thibaut-some-135408a6/
    icon: fa-linkedin
    name: Linkedin
bio: |
  Je suis un prophète du bon sens en entreprise et SCRUM Master sur le papier. Chez Azaé je viens participer à rendre notre monde plus égalitaire et humain. Toujours en quête d'amélioration, j'encourage mes équipe à devenir les meilleures du monde et ce en se respectant elles-mêmes, ainsi que leur environnement.
outputs:
  - html
  - vcard
like: |
  Me remettre en question, le Japon, les jeux de cartes et jeux vidéo.
---
