---
nositemap: true
name: Quillé Julie
vcard_name: "Quillé;Julie"
email: jquille@azae.net
id: jquille
mobile: "+33635589381"
cv: quille_julie.pdf
organisations:
  - name: Réseau Libre-entreprise
    logo: /assets/images/logo/logo-libre-entreprise.png
    url: https://www.libre-entreprise.org/
social:
  - url: https://gitlab.com/Makoti
    name: Gitlab
    icon: fa-gitlab
  - url: https://www.linkedin.com/in/juliequille/
    name: Linkedin
    icon: fa-linkedin
bio: |
  Passionnée par l’être humain, la psychologie et la sociologie, j’accompagne les personnes et les équipes dans leur cheminement vers plus de responsabilité et de liberté individuelles. La Communication Bienveillante (CNV) est mon outil de prédilection.
  Dans une dynamique d'amélioration continue, il me tient à cœur d’élargir en permanence mes domaines de compétence, par des lectures, des recherches, des formations, mais surtout à travers des échanges et de l’expérimentation.
  Ce sont mes intérêts pour l’épanouissement au travail, l’adaptabilité et le travail collaboratif qui m’ont amenée à l’agilité et au coaching d'équipes.
outputs:
  - html
  - vcard
like: La Communication NonViolente, le soleil, le thé, me promener.
resources:
  - name: cv.pdf
    src: 'cv.pdf'
---
