---
title: La communication consciente
description: Apprendre comment communiquer avec bienveillance
category: peoples
illustration:
  name: cover.jpeg
tags: [communication]
duration_:
  days: 1
  hours: 7
presentation: |
  Cette formation permet de découvrir des techniques de communications, inspirées de la Communication Non violente.
objectives: |
  - Connaître l’origine de la Communication Non Violente
  - Connaître le processus OSBD
  - Comprendre les mécanismes de la colère et le lien avec la violence
  - Etre en capacité de pratiquer l’écoute de soi et l’écoute de l’autre
prerequists: Aucun prérequis
duration: La formation est dispensée sur une journée de 7 heures
resources:
  - name: Programme de formation
    url: programme-formation-communication-consciente.pdf
  - name: CGV
    url: /assets/documents/Azae-CGV.pdf
---
