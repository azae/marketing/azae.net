---
date: 2023-08-04
title: "Arpentage en Meetup : une expérience de lien et d’apprentissage"
draft: false
authors:

themes:
  - Meetup
  - apprentissage
  - arpentage
  - engagement

illustration:
  name: arpentage.jpg
description: |
  Nous nous retrouvons à 7 autour d'un livre, nous nous regardons, nous n'avons pas commencé mais nous le savons le livre ne sortira pas de ce meetup en un seul morceau ! En effet, c'est l'arpentage que nous nous appretons à découvrir ensemble, l'intégrité physique du livre en est le prix, mais le jeu en vaut le chandelle.
---

## L’arpentage : un outil d’éducation populaire

### Le choix d’un modèle

Que ce soit à travers nos lectures, nos valeurs ou nos croyances, tout nous ramène au même point : nous ne ferons rien si nous ne faisons pas ensemble. C’est cette conviction forte qui nous motive à regarder de plus près les outils d’éducation populaire, outils qui ont vocation à rendre accessibles à toustes les moyens d’apprentissage, de réflexion et de modélisation du monde. 

L’arpentage est un des outils qui a retenu notre attention et nous a donné envie de le découvrir et de l’expérimenter. C’est lors de l’anniversaire des 5 ans de NousToutes que nous avons découvert l’arpentage et surtout cette manière de l’animer. Nous pouvons trouver de nombreuses façons de le vivre, nous avons été touché.es par celle-ci, et bien que curieux.ses d’explorer d’autres pistes, c’est avec gratitude que nous avons choisi de suivre la proposition qui nous a été présentée lors de l'atelier pour notre animation (nous reviendrons dessus plus tard dans cet article).



### Arpentages : points communs

Issu des milieux ouvriers dans une démarche de réappropriation collective du savoir en cassant notamment la barrière de l’accès à la lecture d'essais (potentiellement complexes) grâce au collectif, . L’arpentage a ensuite poursuivi son chemin dans différents milieux, en général plutôt engagés et militants.

Il y a donc une démarche de désacralisation du livre, et à travers ça du savoir et de la connaissance, qui est intrinsèque à l’exercice. Loin de se dire qu’on va être accompagné·e pour comprendre (relation sachant.e – apprenant.e), l’idée est de se dire qu’ensemble on est en mesure d’attaquer des textes complexes quelque soit notre milieu culturel. En somme, pratiquer l'arpentage c’est déjà faire groupe pour être plus fort.es ensemble, et potentiellement dépasser des limites qui nous auraient semblées infranchissables seul.es.

Et bien qu’il puisse y avoir des variantes sur la manière de mener l’échange, il semble que l’accueil du niveau de connaissance de chacun.e sur un sujet reste une constante. Loin d’un exercice de valorisation de soi, l’arpentage est un outil d’émancipation individuelle à travers le collectif, afin de retrouver sa liberté d’être et de penser sans demander d'autorisation. Ainsi, nous retrouvons souvent l’invitation à partager sa propre expérience, ses ressentis, ses jugements ou encore les liens que nous pouvons faire avec ce que nous vivons dans le quotidien. Faire le lien entre le vécu individuel et le politique semble être, si ce n’est de manière prescriptive a minima de manière descriptive, un des nombreux effets de l’arpentage.

 

## Animation et apprentissages

### Un meetup pour expérimenter

Mardi dernier, c’est à 7 que nous nous sommes retrouvé.es dans l’intimité d’un salon privé pour découper _Désirer à tout prix_ de Tal Madesta. Une première pour la quasi-totalité des participant.es, très intrigué.es par l’exercice.

Le format a été célébré comme simple et efficace (2h30 d'arpentage, suivi d'un "after" pour celleux qui le souhaitaient) :

- Mise en contexte de l’exercice de l’arpentage (nous ne nous sommes pas attardé.es sur le choix du livre qu’aucun.e d’entre nous connaissait)
- Mise en commun de l'ouvrage (partage du titre, auteur et 4ème de couverture, il peut être également bienvenu de laisser en libre accès le sommaire et, le cas échéant, le lexique)
- Découpage du livre par chacun·e (nombre de pages total divisé par le nombre de personnes présentes ; on fait passer l'ouvrage pour que chacun.e prenne son "feuillet")
- 45 min de lecture individuelle (ici, nous avions 18 pages par personnes ; le temps est figé, il n'est pas important que tout le monde finisse sa partie, nous faisons ce que nous pouvons faire dans le temps donné)
- Tour de table de 45 min de retours sur la lecture qui vient d'être réalisée en silence. Dans l’ordre des chapitres, 6 min par personne pour partager une rapide synthèse de ce qui nous a marqué, ainsi que nos ressentis, jugements et sentiments par rapport au morceau de texte que nous avons lu
- Tour de table de 15 min pour partager ses impressions sur l’exercice de l’arpentage : 2 min par personne pour partager notre vécu

- En bonus : 1h30 d'after 😊 : celles et ceux qui le souhaitaient (dans notre cas tout le monde) ont choisi de prolonger les discussions avec des échanges sur des points précis du livre, des vécus et questionnements personnels, notre rapport à l’ouvrage, mais aussi échanger sur notre rapport aux livres en général.

 

### Ce que nous en retenons

Nous ressortons globalement avec une grande joie du format et des échanges, sans doute une expérience à réitérer !

Les + :

- Impression d’avoir réussi à faire le tour d’un livre en un temps relativement court
- Belle expérience de désacralisation de l'objet livre
- Création d’un espace d’échange et de partage avec une possibilité de se connaître un peu les un.es les autres, avec nos spécificités propres
- Une belle façon de vivre des espaces politisés
- La distinction entre l’arpentage à proprement parler (qui permet un cadre sécurisé pour partager ni plus ni moins que ce que l’on souhaite), et l’after (où l’échange est plus libre), nous semble être un bel équilibre

Les - :

- Dans le groupe que nous étions, le tour de table dédié à la synthèse ET au partage de notre vécu et ressentis n'a finalement pas laissé la part belle au "soi" : c'est avant-tout les propos de l'auteur qui ont été résumés, le ressenti de chacun.e a été au final assez secondaire. Malgré des échanges d’une grande richesse, et le fait que ce format est parfois recherché, nous avions l’intention de minimiser l’impact du capital culturel, en essayant de nous éloigner un peu des exercices « scolaires »
- L’exercice est addictif et donne très envie de recommencer 😊

Nous avons hâte de pouvoir expérimenter à nouveau. Si l’exercice vous donne envie et que vous souhaitez être informé·e des prochaines sessions, n’hésitez pas à rejoindre le [meetup Cultural Refactoring](https://meetu.ps/c/4XlVV/6yXB6/a).


## Ressources

- [#NousToutes](https://www.noustoutes.org/) qui nous a permis de découvrir le format
- [Arpentage (éducation populaire) — Wikipédia](https://fr.wikipedia.org/wiki/Arpentage_(%C3%A9ducation_populaire))
- [Meetup Cultural Refactoring](https://meetu.ps/c/4XlVV/6yXB6/a)
