---
date: 2018-11-27
title: "Nouvelle expérimentation d'intelligence collective"
draft: false
authors:
  - quille_julie
themes:
  - intelligence collective
  - CNV
outils :
  - WalkingDev

illustration:
  name: paris-06-11.jpeg.jpeg
description: |
  Forum ouvert, world café, pédagogie inversée, nous voyons émerger de toutes parts de nouvelles façons de partager, d'apprendre, de réfléchir. L'intelligence collective, fil rouge de toutes ces innovations, n'a pas dit son dernier mot. Aujourd'hui je vous parle du walkingdev, ce format de partage où l'on allie le plaisir de la tête au plaisir du corps.
---


Forum ouvert, world café, pédagogie inversée, nous voyons émerger de toutes parts de nouvelles façons de partager, d'apprendre, de réfléchir. L'intelligence collective, fil rouge de toutes ces innovations, n'a pas dit son dernier mot. Aujourd'hui je vous parle du walkingdev, ce format de partage où l'on allie le plaisir de la tête au plaisir du corps.

## Le format

Une journée, une ville, un thème, des organisateurs, des participants et beaucoup de plaisir. Voici les ingrédients d'un walkingdev. Ce format, qui m'était totalement inconnu il y a peu, m'a tout de suite intriguée. Dans le teaser c'était annoncé : "pas de sachants, seulement des personnes qui ont envie de partager et d'apprendre, et ça fonctionne".
Ma curiosité ainsi que ma confiance dans les formats innovants et les organisateurs, m'ont convaincue de réserver mon mardi 6 novembre pour aller marcher avec un petit groupe de personnes pour un [WalkingDev Communication NonViolente](http://walkingdev.fr/#walkingdev/cnv/blob/master/v-75/faq.md).

## Plaisirs corporels

On travaille l'ensemble des besoins de la pyramide de Maslow, et, c'est étudié, on commence par la nourriture ! On se retrouve en début de matinée, dans un petit café pour prendre pâtisseries et boissons chaudes. Le contrat est clair, aujourd'hui on se régale. Ce ne sera que le début, la suite de la journée nous amènera, entre autre, à la grande mosquée de Paris où nous dégusterons couscous et petits gâteaux. Partie gourmande de la journée grandement assurée.
Entre chacune de ces étapes, nous nous déplacerons à pieds, cela fait partie intégrante de l'expérience : on mange, on bouge, on découvre. Une jolie manière de favoriser les échanges avec tout un chacun.

## Plaisirs cérébraux

Ne l'oublions pas, c'est annoncé dans le titre, le walkingdev est avant tout une expérience d'intelligence collective. Nous sommes tous ici pour découvrir ou approfondir nos connaissances sur la Communication NonViolente (CNV). On commence la journée en posant nos intentions pour la journée, on poursuivra par une introduction à la CNV, des exemples d'expression authentique, des jeux, la liste est longue. Entre chacun de ces "ateliers", les échanges informels nous permettent de partager nos points de vue et expériences teintées de la couleur de chaque participant. Dans tous ces échanges, chacun maîtrise la CNV à son échelle. Il n'y a pas de formateur, chacun est invité à s'approprier cette journée et à participer à la construction de son programme.

## Mon bilan

Personnellement, j'ai beaucoup apprécié ce format, ainsi que le confort de pouvoir se reposer entièrement sur les organisateurs pour déambuler dans Paris. J'ai également apprécié le fait d'être en mouvement. Il me semble que c'est un format qui rejoint les mouvances qui mettent le corps en mouvement.
J'ai également pu expérimenter une fois de plus le challenge qu'est l'auto-organisation. En effet, bien qu'étant tous plutôt aguerris à ce type de fonctionnement, nous pouvons constater que nous sommes dans une démarche d'expérimentation et d'innovation : il y a encore du chemin. Un pas de plus vers le "toujours plus de conscience", merci à tous les particpants et aux organisateurs pour ce beau moment d'apprentissage.
