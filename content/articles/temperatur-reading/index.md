---
title: Lecture de la température
date: 2016-03-14
authors:
  - clavier_thomas
  - albiez_olivier
outils:
  - conseil
illustration:
  name: illustration.jpg
aliases:
  - articles/temperatur-reading.html
---

## Temperature reading ou conseil par [Virginia Satir]

C'est une forme de rétrospective de groupe, que Virginia Satir proposait aux familles qu'elle accompagnait. C’est une réunion où les personnes se parlent et font l’expérience individuelle d’appartenir à un groupe.
Elle proposait de le faire une fois par semaine en famille afin de garder un rythme d'amélioration continue.
Nos amis d'[/ut7] l'appellent « le conseil ».


### Les rôles

* **Un guide** qui va s'assurer que les règles sont respectées, il annonce les étapes dans l'ordre et peut aussi jouer le maître du temps.
* **Les participants**, tout le monde y compris le guide.


### Le déroulement

Il est découpé en 5 étapes :

1. **Une appréciation positive** : partager 5 mercis à d'autres personnes du groupe pour quelque chose que vous avez apprécié de leur part depuis le dernier conseil.
2. **Les plaintes et les peurs** : décrire 2 choses que vous espérez à long terme et à court terme. Accompagner votre plainte d'un plan d'action personnel.
3. **Les puzzles** : expliciter ce que l'on ne comprend pas dans le groupe, les mystères et les rumeurs.
4. **Les informations et les nouvelles** : partager les faits nouveaux.
5. **Les souhaits, les espoirs et les rêves** : exprimer ce que l'on aimerait, c'est l'occasion de partager sa vision, tout en sachant qu'elle ne va pas forcément s'incarner.

Le guide peut alors cloturer de manière explicite le conseil.

### Comment ça marche ?

La première étape permet de mettre le groupe dans une dynamique positive. Nous avons souvent plus l'habitude de nous plaindre que de dire merci.

La deuxième étape permet de partager les rêves. Un partenaire qui comprend vos rêves est en mesure de les aider à se produire. En accompagnant votre plainte, espoir ou rêve d'un plan d'action personnel, vous déchargerez le groupe du sentiment de responsabilité sans l'empêcher de vous aider. Par exemple : j'aimerais ne pas manger de la soupe tous les soirs, je vais préparer le dîner demain soir.

La troisième étape permet d'exprimer les non-dits et d'éviter les frictions futures.

La quatrième étape permet de finir sur un partage d'informations pour homogénéiser le niveau d'informations.

La dernière étape permet de partager ses rêves et accepter une éventuelle frustration.

## Conclusion

Cette forme de rétrospective démarre sur une dynamique très positive avant d'aborder les actions personnelles d'amélioration et conclut sur la vie du groupe. En mettant en avant des actions personnelles, on implique chaque membre du groupe à s'engager dans l'amélioration continue, sans exclure une réelle dynamique de groupe. Elle est particulièrement adaptée aux équipes qui ne sont pas totalement alignées.


[Virginia Satir]: https://fr.wikipedia.org/wiki/Virginia_Satir
[/ut7]: https://ut7.fr/blog/2015/11/18/animer-vos-retrospectives-avec-le-conseil.html
