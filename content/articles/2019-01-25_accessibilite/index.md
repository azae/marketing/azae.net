---
date: 2019-01-25
title: "Automatiser les contrôles qualité de notre site"
draft : false
authors:
  - clavier_thomas
vie d'Azae:
  - interne
description: |
  Produire un site web de qualité, ce n'est pas simple. Et si l'on automatisait une partie des contrôles ? Après une rapide description des contrôles que l'on cherche à faire, j'exposerai en détail la mise en œuvre.
illustration:
  name: handicapped.jpg
---

Produire un site web de qualité, ce n'est pas si simple. D'ailleurs, on entend quoi par qualité ? Et si l'on automatisait une partie des contrôles ? Après une rapide description des contrôles que l'on cherche à faire, j'exposerai en détail les outils utilisés et leur configuration.

# Où l'on parle de qualité

Sans revenir sur les concepts de qualité intrinsèque et extrinsèque, sans détailler les notions de qualité esthétique et technique, voici les quelques éléments que je cherche à tester automatiquement :

1. Vérifier que tous les liens à l'intérieur du site sont bien valides et fonctionnels.
1. Vérifier que les liens vers les autres sites sont encore d'actualité.
1. Vérifier que notre site respecte bien les normes du [W3C](https://validator.w3.org/).
1. Vérifier que notre site est bien lisible par tous.

Ce dernier point est sans conteste un des plus importants. Chez Azaé, le respect des minorités est capital. Nous trouvons très important de pouvoir accueillir tout le monde sans exception. Mettre de côté une partie de la population sous couvert de "Ça ne représente pas une grande part de nos clients" nous parait juste impensable.

Vous l'aurez compris, on parle ici de critères de qualité intrinsèque en partie visible par vous, lecteurs.

# Comment marche notre site

Note site web est propulsé par [hugo](https://gohugo.io/) c'est-à-dire que nous avons d'un côté le contenu sous forme de fichiers Markdown complété de quelques méta-données en Yaml, et de l'autre des squelettes de pages en html.
Le tout étant archivé dans [git](https://gitlab.com/azae/marketing/azae.net/).
Hugo va utiliser ces deux sources pour produire l'ensemble des pages du site.
Les pages ainsi produites sont empaquetées dans un container docker avec un serveur nginx afin de pouvoir servir nos pages web depuis n'importe quel serveur public docker, en l'occurrence nous utilisons notre propre plateforme [deliverous](http://deliverous.com).

La phase de construction se fait avec [gitlab-ci](https://docs.gitlab.com/ce/ci/yaml/README.html) qui va lancer une construction docker, qui va lui-même lancer la génération des pages.

```gitlab-ci
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t $CONTAINER_NAME .
    - docker push $CONTAINER_NAME
```

Le [Dockerfile](https://gitlab.com/azae/marketing/azae.net/blob/master/Dockerfile) est disponible en ligne ainsi que le [container](https://gitlab.com/azae/marketing/azae.net/container_registry) généré.

# Validons les liens

Il est relativement simple de vérifier la validité des liens d'un site, des crawleurs comme [linkchecker](https://wummel.github.io/linkchecker/), en ligne de commande, ou d'autres, en ligne, permettent de tester un à un l'ensemble des liens d'un site web.

Une des bonnes pratiques du déploiement continu consiste à bloquer le déploiement à la moindre erreur. C'est jouable pour tous les liens internes qui sont sous notre contrôle, mais moins pour les liens externes qui ne dépendent pas que de nous. J'ai donc opté pour bloquer le déploiement en cas de lien interne cassé. Je n'ai pas encore de solution pour alerter de la présence de liens extérieurs erronés.

Pour ce faire, gitlab-ci lance notre container précédemment construit et le rend accessible sur l'url http://azae/. Puis gitlab-ci lance linkchecker. Pour lancer linkchecker j'ai construit un [container](https://gitlab.com/azae/docker/blob/master/Dockerfile.linkchecker) avec tous les éléments nécessaires à son bon fonctionnement.


```gitlab-ci
linkchecker:
  stage: test
  image: registry.gitlab.com/azae/docker/linkchecker
  services:
    - name: $CONTAINER_NAME
      alias: azae
  script:
    - linkchecker --anchors --ignore-url="^mailto:" http://azae/
```

# Une histoire d'accessibilité

Les premières questions que je me suis posées pour tester l'accessibilité de notre site tournaient autour des critères mesurables de l'accessibilité.
Mes recherches m'ont mené vers [pa11y](http://pa11y.org/). J'ai découvert au passage qu'il est d'usage de parler d'a11y qui fait référence au mot "accessibility", soit onze lettres encadrées de "a" et "y".

De la même façon que pour linkchecker, gitlab-ci lance le container de notre site et le rend accessible avec l'url http://azae/ puis lance pa11y grâce à [un container idoine](https://gitlab.com/azae/docker/container_registry). Ce dernier contient un fichier de configuration par défaut que l'on doit explicitement charger.

```gitlab-ci
accessibility:
  stage: test
  image: registry.gitlab.com/azae/docker/pa11y
  services:
    - name: $CONTAINER_NAME
      alias: azae
  script:
    - pa11y-ci --sitemap http://azae/sitemap.xml --config /etc/pa11y.conf --sitemap-find azae.net --sitemap-replace azae --sitemap-exclude pdf
```

# Et après

Nous venons d'initier ici un ensemble de tests automatiques pour garantir l'accessibilité de notre site au plus grand nombre. Nous avons aussi répondu à un besoin très fort chez nous, celui de la recherche de l'excellence. Produire des pages web de qualité respectant au mieux les gens et les standards.

Notre quête n'est pas terminée, il nous reste par exemple la validation des pages web par les outils du [W3C](https://validator.w3.org/) et la vérification des éléments open graph que nous publions.
