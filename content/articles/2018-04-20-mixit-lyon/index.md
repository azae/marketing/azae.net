---
date: 2018-04-20
title: "Voyage en MixIT 2018"
draft: false
authors:
  - quille_julie
vie d'orateur:
 - MixIT
themes:
  - aide infligée

illustration:
  name: mixit.png
description: |
  Des trains en grève, des voitures en panne, des collègues bloqués, ma première édition de MixIT Lyon était déjà une aventure avant même que de commencer. Mais rien ne m’arrête, avec mon gros ordinateur sous Debian et ma petite valise rouge, me voilà traversant la France pour me retrouver en terre inconnue : arrivée à MixIT prête à tout pour assurer notre talk sur l’aide infligée !
---

Des trains en grève, des voitures en panne, des collègues bloqués, ma première édition de [MixIT Lyon](https://mixitconf.org/fr/) était déjà une aventure avant même de commencer. Mais rien ne m’arrête, avec mon gros ordinateur sous Debian et ma petite valise rouge, me voilà traversant la France pour me retrouver en terre inconnue : arrivée à MixIT prête à tout pour assurer notre talk sur [l’aide infligée](http://ajiro.fr/talks/aide_infligee/) !

On connaît un peu la chanson, un CFP, une série de conf, des salles, des gens sympas, des échanges au top, on ressort épuisés mais ravis, c’est presque devenu la routine pour tous les amateurs des rencontres Agiles en tout genre, mais là… là je découvre une multitude de petites choses qui chacune d’elle aurait pu faire de cet événement un rendez-vous annuel auquel je voudrais absolument participer, et tout ensemble Waouh !!


## Des sous-titres en veux-tu en voilà

<img src="sous-titres.jpg" class="img-fluid" alt="Sous titres"/>

D’abord toutes les conférences auxquelles j’ai assisté étaient sous-titrées pour les rendre accessibles aux sourds et malentendants. Vous allez peut-être avoir envie de me demander, « et du coup combien de personnes concernées étaient présentes ? » Et je vous répondrais volontiers « Il n’y avait pas une seule personne présente non concernée ! ». Dans tous les cas c’est l’impression qu’a réussi à me faire passer l’organisation de MixIT dans le peu de temps que j’y ai passé : tout un chacun est porteur de la prise en compte des minorités. Et ils l’ont fait, on change de paradigme, fini les « dites-nous si vous venez, nous nous organiserons a posteriori » et voici les « nous sommes accessibles a priori ». Normalement, à ce stade de l’article, je vous ai déjà convaincus, vous êtes sous le charme et n’avez interrompu votre lecture que quelques secondes, le temps d’aller regarder les dates de MixIT 2019 pour bloquer vos dates ! Et vous avez raison (d’être allé voir, parce que les dates vous ne les trouverez pas tout de suite), mais ce n’est pas tout.


## Des talks randoms mystère

Nous le voyons, le monde de l’agilité évolue, la parité dans l’IT est devenu un sujet qui ne fait plus exception, l’agilité hors IT est régulièrement abordée et j’adore. Mais là encore MixIT va un cran plus loin, Des sessions randoms sont proposées. Qu’est-ce que cela veut dire ? Et bien c’est simple : vous allez dans une salle et vous assistez à un talk de 20 min, le temps de voyager dans un nouvel espace de votre système de pensée, on fait jouer le hasard et le mystère. Vous penserez peut-être « ouais mais moi... », ne finissez pas votre phrase, toute l’idée est là ce n’est pas « moi » qui choisis, pas mon système de pensée, pas mes mécanismes de sélection qui sont mis en action. Il fallait oser, et en terre de découvertes et de surprises, moi je dis « eureka ».


## Les crêpes de Raph’

Bon, là, on va dans le détail (mais nous le savons tous, ce sont les détails qui font les vraies différences), des crêpes à volonté, toute la journée. Vous vous dites détail, je vous dis génie. La preuve, je n’étais pas encore arrivée à MixIT, et j’avoue je n’avais qu’une idée très floue de l'événement, qu’un ami de la capitale me dit « Tu as goûté les crêpes de MixIT ? ». Alors quand même une conférence IT qui arrive à se faire reconnaître à ses crèpes, si là nous n’avons pas atteint le summum de la vulgarisation je ne sais pas ce qu’ils pourraient faire de plus !


## Vous arrive-t-il d’infliger de l’aide

<img src="public.jpg" class="img-fluid" alt="Audiance"/>

Oui quand même, je ne pouvais pas ne pas en parler parce que c’est quand même pour ça que j’y étais à la base ! Et ce fut une très belle expérience avec une salle comble pour un sujet de SoftSkills, un vrai bonheur. En tant que speakeuse j’ai trouvé l'accueil excellent, toute l’équipe de bénévoles géniale, accessible et aidante : un vrai plaisir. Et j’ai même eu l’honneur d’être sketchnotée, et vous le remarquerez « I made my point ».

<img src="sketchnote.jpg" class="img-fluid" alt="Sketch note"/>


Vous l’aurez compris à travers ces petits points non exhaustifs, j’aime les conférences Agile et je suis plutôt coup de coeur que coup de gueule, mais malgré tout l’amour que j’ai pour les événements et les personnes que je rencontre, MixIT a pour l’instant une place toute particulière dans mes BestConfEver. Pour finir juste un grand MERCI à toute l’orga qui nous a bichonnés pendant cet événement.
