---
date: 2023-12-03
title: "Un revenu de base dans l'entreprise : à quoi ça pourrait ressembler ?"
draft: true
authors:
  - quille_julie
themes:
  - argent
  - rémunération
  - revenu de base
  - engagement
  - méritocratie

illustration:
  name: money.jpg
description: |
  Depuis un peu plus d'un an maintenant nous expérimentons le Revenu de Base au sein d'Azaé. Plus lieu d'expérimentation que lieu d'industrialisation, c'est en mode POC que nous avons souhaité abordé le sujet. Bien que l'enjeu économique n'est pas de taille, on a pu avoir accès à une grande richesse de vécus et d'apprentissages. On vous en dit un peu plus dans cet article.
---

Depuis un peu plus d'un an maintenant nous expérimentons le Revenu de Base au sein d'Azaé. Plus lieu d'expérimentation que lieu d'industrialisation, c'est en mode POC que nous avons souhaité abordé le sujet. Bien que l'enjeu économique n'est pas de taille, on a pu avoir accès à une grande richesse de vécus et d'apprentissages. On vous en dit un peu plus dans cet article.

## Que mettons nous derrière le terme de Revenu de Base ?

Le revenu de base dont nous rêvons s'inscrit pleinement dans la démarche du [Mouvement Français pour un Revenu de Base](https://www.revenudebase.info/decouvrir/). Ainsi nous souhaitons un revenu de base qui respectent un certains nombre de critères :
    - Universel : chaque membre de la communauté doit le toucher
    - Inconditionnel : il n'est pas lié à une quelconque action ou comportement spécifique, le seul critère : appartenir à la communauté
    - Par et pour les membres de la communauté : nous mettons un point d'honneur à ce que le revenu de base soit financé uniquement par les membres qui le reçoivent

En résumé, c'est une somme d'argent que nous versons automatiquement à chaque personne appartenant au groupe, indépendamment de la manière dont elle a utilisé son temps et de sa contribution reconnue par le groupe.

Evidemment la manière dont nous définissons la communauté impacte fortement notre version de revenu de base. Nous y reviendrons plus tard.

## Le revenu de base en tant que stratégie de groupe

Le mise en place d'un revenu de base au sein d'Azaé vient soutenir deux enjeux majeurs. D'une part, augmenter la sécurité financière de chaque personne afin de réduire la peur du manque et les comportements que cela induit. D'autre part, travailler sur le système de croyance méritocratique et réussir à créer autre chose.

Dans notre expérience de groupe nous avons déjà remarqué que les systèmes de croyances sont activés quelque soit la somme mise en jeu. Au delà des enjeux qui nous viennent facilement en tête lorsque nous parlons d'argent, par exemple la reconnaissance ou la sécurité financière, c'est aussi les enjeux en lien avec l'estime de soi, le sentiment de justice, les systèmes de pouvoirs et de tension qui sont mobilisés dès qu'on touche à l'argent.
En simplifié, nous pouvons avoir des enjeux fort sur qui va touche 1€ du moment que nous cherchons à savoir qui le mérite le plus.

C'est pour ces raisons que nous avons choisi de partir sur un revenu de base modeste, mais non nul. Cela nous permets dans un premier temps de dissocier les enjeux en lien avec la sécurité financière des autres, afin de pouvoir les travailler avec un maximum de sécurité. Un peu de petit bain avant de plonger en haute mer.

# Concrètement comment ça fonctionne

## Les règles du revenu de base d'Azaé en version simpliste

Nous pouvons envisager notre communauté comme un groupement de 3 personnes qui fonctionnent en free-lance (fonctionnement qui resemble à des auto-entrepreneureuses).
Le montant du revenu de base que nous avons actuellement est de 1 000€.
Chaque mois nous devons donc obtenir 3 000€ pour pouvoir verser le revenu de base. Nous les récupérons en prenant un pourcentage de ce que chaque personne à facturé jusqu'à ce que nous ayons les 3 000€.

Exemples :
![Exemple avec 3 personnes : A gagne 3000€ B gagne 0€ et C gagne 1500€ après cotisation au revenu de base (environ 67% pour tout le monde), et redistribution avec un revenu de base de 1000€, A touche 2000€, B touche 1000€ et C touche 1500€](exemple2.jpeg "Exemple revenu de base avec 3 personnes mois 1")

## Les règles en vrai (et en un peu plus compliqué) :

Chaque membre de cette communauté étant co-gérant, lorsque nous parlons aujourd'hui de 1 000€ il s'agit de 1 000€ de chiffre d'affaire. Avec notre fonctionnement actuel cela équivaut à environ 450€ de revenu net imposable.
Par ailleurs, nous ne sommes pas en capacité de faire ces calculs sur le mois. Ainsi il serait plus juste de dire que nous avons un revenu de base annuel autant dans la manière dont on y contribue que dans la manière de la recevoir.
Nous avons d'autres systèmes en place pour assurer de toucher un minimum mensuel, indépendamment de ce que nous avons gagné avec un système de lisage collectif.

# Les étapes essentielles

Il nous est possible de mettre en place ce système pour deux raisons principales :
    - nous sommes privilégiés de part le groupe social que nous constituons (emploi que nous avons, temps dont nous disposons pour construire le collectif)
    - nous créons communauté autour d'un désir partager de travailler à déconstruire la méritocratie et l'envie de faire groupe à travers notamment des discussions autour de ce qui nous challenge dans le fait de faire groupe

## Faire groupe

La première étape à donc été de définir ce qu'était notre communauté, le groupe au sein duquel nous souhaitions mettre en place le revenu de base et donc un mécanisme de groupe basé sur l'interdépendance.
Aujourd'hui certaines règles s'appliquent :
    - la majorité (voir l'intégralité) de nos revenus doivent provenir d'Azaé
    - nous participons activement à la vie d'entreprise et aux instances de régulation
    - partager un conscience partagée des enjeux de l'interdépendance

Du fait que notre revenu de base s'inscrit dans le cadre d'une entreprise, certains critères de revenu de base sociétal ne s'appliquent pas. Par exemple, le fait de recevoir le revenu de base est conditionné à l'appartenance au groupe. Dès que la personne va sortir de l'entreprise (ou avant qu'elle n'y rentre), elle ne recevra pas de revenu de base.
 De la même manière une personne qui cumulerait les "emplois" ne rentrerait pas dans nos critères de bénéficiaire.
Les limitent sont très en lien avec les leviers d'action que nous avons pour que le revenu de base dont nous parlons soit bien par et pour les membres de la communauté. Ainsi nous mettons de l'attention aujourd'hui à ne pas financer notre revenu de base avec le travail de personne qui n'en sont pas bénéficiaire, et réciproquement, que les personnes qui ne contribuent pas au revenu de base avec les règles émises par le groupe, n'en soit pas bénéficiaire.

## De l'enjeu ET de la sécurité

Nous avons choisi de priorisé la déconstruction des systèmes de croyance, comme première étape pour pouvoir mutualiser la sécurité financière.
Dans ce cas nous avons mis en place plusieurs choses :
    - un montant de revenu de base qui ne mettais en péril la sécurité de personne (nous nous rappelons que cotiser à un revenu de base n'est pas forcément accessible à tout le monde)
    - des espaces de discussion autour de ce que nous vivons lorsque nous vivons cette expérimentation (aujourd'hui nous passons 2 jours ensemble tous les 3 mois pour parler de la vie d'entreprise en général, dont les sujets en lien avec l'argent)
    - une possibilité de faire évoluer les règles de fonctionnement

# Le revenu de base, un pierre dans l'édifice

Nous vous avons déjà parlé d'autres expérimentations que nous faisions autour de l'argent et de la méritocratie, par exemple l'existence du [PAP](https://azae.net/articles/2023-07-10_discutons_argent/).
Par ailleurs le revenu de base tel que nous le vivons aujourd'hui est une première étape, qui a ses avantages et ses limites. Nous avons déjà parlé des avantages, en voici quelques limites :
    - la taille du groupe fait que nous avons une résilience assez faible. Par exemple une personne qui n'aurait pas de facturation pendant un an aurait un impact élevé sur les deux autres personnes
    - en lien avec le premier, nous n'avons pas les capacités aujourd'hui de mettre en place un revenu de base qui laisserait réellement l'opportunité à chaque personne de choisir ce qu'elle souhaite faire de son temps sans se soucier de ses besoins fondamentaux
    - nous avons conscience que le système actuel ne fonctionnent que parce que nous avons une quantité de privilèges assez grande
    - nous sommes fortement impactés par les règles extérieures à l'entreprise (par exemple une personne sans emploi pourrait être pénalisée du fait de son appartenance au groupe qui lui couperait ses droits chômage par exemple)

Nous espérons que la suite de nos aventures ira dans le sens de réduire ces limites et offrir de nouvelles perspectives (et de nouvelles difficultés sans doute), à notre fonctionnement.
