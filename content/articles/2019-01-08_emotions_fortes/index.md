---
date: 2019-01-08
title: "Article Emotions Fortes"
draft : false
authors:
  - quille_julie
vie d'Azae:
  - publication
outils :
  - émotions
  - besoins
  - CNV
description: |
  Lors d’Agile Tour Lille dont nous vous parlions il y a quelques semaines, nous avons présenté l’atelier Les émotions fortes au travail : qu’est-ce qu’on en fait ?. En ce début d’année 2019, nous vous mettons à disposition la description de l’atelier Emotions fortes pour que vous puissiez le refaire à volonté.
illustration:
  name: atelier-emotion.png
---

Lors d'[Agile Tour Lille](http://azae.net/articles/2018-11-13_agile_tour_lille/) dont nous vous parlions il y a quelques semaines, nous avons présenté l'atelier [Les émotions fortes au travail : qu'est-ce qu'on en fait ?](http://ajiro.fr/talks/atelier_cnv/).
En ce début d'année 2019, nous vous mettons à disposition la description de l'atelier [Emotions fortes](http://ajiro.fr/games/emotions_fortes/) pour que vous puissiez le refaire à volonté.

Petit plus : le [support Agile Tour Lille](http://ajiro.fr/talks/atelier_cnv/2018-11-08_emotions_travail.pdf)
