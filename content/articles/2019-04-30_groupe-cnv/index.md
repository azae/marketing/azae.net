---
date: 2019-04-30
title: "Groupe de pratique CNV : premiers ateliers chez Azaé"
draft: false
authors:
  - quille_julie
vie de coach:
  - animation
  - formation
outils:
  - CNV
illustration:
  name: cnv_martinique.jpeg
description: |
  Nous sommes heureux de vous dire aujourd'hui que la Girafe Azaé, née à Nanterre, s'est réuni pour la première fois en mars et que les prochaines dates sont fixées !
---

Vous connaissez déjà notre passion de la Communication NonViolente. Depuis septembre dernier, nous avons l'intention de créer des groupes de pratique dans l'idée de pouvoir proposer un maximum d'espaces d'échange sur ce sujet que nous affectionnons.
Nous sommes heureux de vous dire aujourd'hui que ces initiatives commencent à porter leurs fruits. En effet, la Girafe Azaé, née à Nanterre, s'est réunie pour la première fois en mars et nous avons fixé les prochaines dates. Nous en profitons pour vous en dire plus.

## Groupe de pratique

Les groupes de pratique en Communication NonViolente (CNV) ont pour vocation de proposer des lieux de pratique et d'échange autour de la CNV. Les règles de fonctionnement et les thématiques varient d'un groupe à l'autre. Certains sont auto-organisés, d'autre sont crées à l'initiative d'une ou plusieurs personnes qui prendront en charge l'animation du groupe, certains sont payants, d'autres non. Vous l'aurez compris, le seul point commun de ces groupes est la passion de cette pratique que nous a transmis Marshall Rosenberg.

L[Association pour la Communication NonViolente (ACNV)](https://cnvfrance.fr/) propose une [carte des groupes de pratique](https://cnvfrance.fr/carte-groupes-de-pratique-cnv/).

## Girafe Azaé - retour d'expérience du groupe de Nanterre

Depuis septembre 2018, nous avons ouvert un [groupe de pratique à Nanterre](https://cnvfrance.fr/carte-groupes-de-pratique-cnv/). Il nous tient à coeur de pouvoir proposer des espaces de pratique ouverts et gratuits qui viennent en complément des formations et autres moments d'échanges plus formels. L'idée est de pouvoir découvrir ou revoir les bases et surtout de PRA-TI-QUER !
La mise en place d'un groupe est toujours un équilibre subtil : disponibilités, intérêts, différences d'envies ou d'expérience sont autant de points qui construisent les richesses et les faiblesses d'un groupe. C'est donc avec une émotion toute particulière, qu'après un an de travail, la première édition de la Girafe d'Azaé a vu le jour le mardi 12 mars 2019.

Ainsi, pour ceux qui veulent découvrir ce groupe de pratique, et éventuellement construire la suite, nous vous donnons rendez-vous aux dates suivantes :

> Mardi 7 mai 2019

> Mardi 4 juin 2019

## Retours sur le 12 mars

Nous nous sommes retrouvés en petit comité à Nanterre pour parler de CNV et parler en CNV. Marshall nous disait que la CNV c'était une langue que nous devions réapprendre, l'objectif du groupe de pratique et de créer des espaces pour le faire. Ainsi nous avons, le temps d'une soirée, retrouvé cette énergie d'apprentissage d'une langue très vivante. Un peu de théorie ; nous embarquons les derniers arrivés dans la grande famille des CNVophones, ensuite pratique et échanges ; nos attentes, nos ressentis, notre pratique. La forme s'allie au fond, nous nous entraînons et nous entraidons dans la joie de l'authenticité.

## Un peu plus sur la CNV

### Avec Azaé

Conférence [La CNV au service de l'agilité](https://www.youtube.com/watch?v=VJnXtoB1RAU)

Description d'atelier sur [les émotions fortes](https://ajiro.fr/games/emotions_fortes/)

### Avec les classiques

Conférence de présentation de [la Communication NonViolente](https://youtu.be/bIjRxdN-kL8) par Marshall Rosenberg

Livre [*Les mots sont des fenêtres (ou des murs) : Introduction à la Communication NonViolente*](https://www.librairiesindependantes.com/product/9782707188793/), Marshall Rosenberg
