---
date: 2019-03-12
title: "Pourquoi le logiciel libre est important pour nous"
draft: false
authors:
  - clavier_thomas
vie d'Azae:
  - nos valeurs
  - Libre-Entreprise

illustration:
    name: gnu.jpg
    source: https://commons.wikimedia.org/wiki/File:Wildebeest_portrait.jpg

description: |
  Pourquoi le logiciel libre est-il important pour nous ? Pour répondre à cette épineuse question, retour sur les fondements du logiciel libre avant de parcourir nos valeurs.
---
Pourquoi le logiciel libre est-il important pour nous ? Pour répondre à cette épineuse question, nous allons faire un retour sur les fondements du logiciel libre avant de parcourir les valeurs qu'ils incarnent. Enfin, nous regarderons en quoi cela correspond à notre vision de l'entreprise.

Un logiciel est libre s'il respecte ces 4 libertés fondamentales :

- Liberté d'utiliser le logiciel comme on le souhaite.
- Liberté d'étudier le logiciel et son code source.
- Liberté de modifier le logiciel.
- Liberté de redistribuer la version d'origine comme les versions modifiées du logiciel.

En remplaçant le mot logiciel par album, données ou œuvre d'art, nous nous apercevons que ce concept de libre s'applique à de nombreux autres domaines, nous parlons alors de culture du logiciel libre.
Faire vivre et partager ce concept, est pour nous quelque chose d'important. En effet, le libre illustre une partie de nos valeurs fondamentales.

Afin de ne pas réduire mon propos au monde du logiciel, dans la suite de cet article, nous utiliserons le terme juridique regroupant l'ensemble des productions soumises au droit d'auteur : œuvre de l'esprit.

# Nos valeurs

Venons en détail sur nos valeurs de groupes. Ce sont ces valeurs qui nous regroupent et nous fédèrent chez Azaé.
Sans décrire comment nous les avons définies, ni comment nous les maintenons, ce qui sera sans doute le sujet d'un autre article, voici nos valeurs :

- Démocratie
- Transparence.
- Partage
- Liberté
- Culture de l'excellence.

## Démocratie

Une organisation auto-organisée cherche forcément à incarner la démocratie, en étant notamment contrôlée par l'ensemble des individus qui la composent. De fait, elle va faire preuve de transparence pour que tout le monde puisse prendre des décisions en toute connaissance de cause. Elle va également laisser chacun de ses membres l'étudier et la modifier afin de la faire vivre selon ses besoins.

De même, chez Azaé, nous utilisons et contribuons à des logiciels libres. C'est pour nous une façon d'incarner nos valeurs jusque dans nos outils.

## Partage

Produire une copie d'une œuvre ne coûte presque rien, en revanche, le temps de travail des gens ayant contribué à sa fabrication a un prix. Souhaitons-nous payer une copie ou payer directement le travail des gens ? La culture libre répond clairement à cette question en disant : nous payons le travail des gens et non pas le produit.

Au-delà du seuil de rentabilité, une société informatique fait des bénéfices issus d'un produit sur lequel elle ne travaille plus, ce qui peut produire un sentiment d'injustice profond. Par exemple, en 2017, Microsoft faisait un résultat net de 21,2 milliards d'euros, ce qui aurait put permettre de verser une prime annuelle de 160 000€ à chacun des 131 000 salariés. Or, à ma connaissance, cet argent sert essentiellement à rémunérer les propriétaires de l'entreprise et non ceux qui la construisent.

Pour nous, incarner la valeur partage, c'est aussi être attentif aux modèles économiques auxquels nous souhaitons contribuer, afin de favoriser les modèles que nous trouvons justes et vertueux.

## Liberté

Dans une logique propriétaire pour faire face à la peur de se faire voler ou tout simplement pour garder un monopole, on protège l'œuvre avec un format propriétaire et des verrous logiciels comme les DRM. Ces verrous sont un frein à l'interopérabilité. Nous considérons que c'est un frein à notre liberté : celle d'utiliser le logiciel que nous souhaitons pour profiter d'une œuvre ou collaborer avec certains partenaires.

Dans cette optique de garantir notre liberté, nous essayons de ne pas promouvoir des sociétés qui imposent l'utilisation d'outils qui ne respectent pas nos quatre libertés fondamentales. Le logiciel libre est, pour nous, une garantie de pouvoir respecter les choix de chacun.

## Culture de l'excellence

La garantie de pouvoir étudier et modifier est nécessaire pour toute co-construction. En effet, comment participer à l'amélioration d'une œuvre sans la connaître ? Sans avoir le droit de la modifier ? Ouvrir la co-construction à l'ensemble de la planète, c'est le meilleur moyen d'approcher la perfection. Comme le dit l'adage : ensemble, nous allons plus loin.

Dans cette idée que le collectif est toujours plus fort que l'individuel, nos productions sont systématiquement sous licence libre et accessibles à tous. Nous sommes convaincus que cette démarche est gagnant-gagnant et que notre valeur ajoutée s'en trouve agrandie.

## Reprendre le contrôle de nos données

Un dernier point qui nous semble essentiel est la possibilité de choisir ce que nous souhaitons faire de nos données.
Nous observons une explosion de l'économie de la donnée dont les utilisateurs sont totalement exclus. Reprendre le contrôle de ses données, c'est s'inclure dans cette nouvelle économie.
Qui dit logiciel libre dit transparence, c'est-à-dire la possibilité d'étudier et de modifier le logiciel que nous utilisons.
Ainsi, nous pouvons garantir que nos données ne sont pas exploitées sans notre consentement. C'est une étape importante pour la reprise de contrôle de nos propres données.

En plus des valeurs déjà citées plus haut, cette possibilité incarne des valeurs telles que la sécurité, la justice, le consentement et bien d'autres encore.

# Conclusion

En passant rapidement sur ces quelques points, nous espérons avoir pu vous partager en quoi le logiciel libre et la culture libre, au-delà d'un produit ou d'un choix économique, c'est avant tout des choix liés à des convictions fortes.
Ces convictions, qui ont su s'incarner dans des communautés autonomes et auto-organisées, nous ont beaucoup inspirés dans la construction de notre entreprise. Nous souhaitons utiliser, contribuer et promouvoir le logiciel libre, mais aussi être une entreprise libre qui réponde à ces mêmes valeurs.
