---
date: 2018-11-20
title: "On travaille le feedback à Axa"
draft: false
authors:
  - quille_julie
vie de coach:
  - accompagnement
outils:
  - feedback
illustration:
  name: feedback-positif-julie.png
description: |
 Invitée à la journée des Scrum Masters chez Axa, j'y ai animé un moment autour du feedback positif. Un atelier à diffuser sans modération.
---

  Pour les plus assidus vous avez sans doute suivi les péripéties du feedback positif, de la création de l'atelier dont je vous parlais il y a quelques semaines dans [L'expérience du feedback positif !](https://azae.net/articles/2018-10-23_atelier_feedback_azae/), à la description de l'atelier [Feedback Positif](http://ajiro.fr/games/feedbackpositif/). Et bien convaincue de l'importance de cette étape, souvent negligée, et répondant à une invitation qui m'intéressait particulièrement, j'ai pu profiter de la journée Scrum Masters d'Axa pour jouer l'atelier en grand format.

  Des plaisirs d'une journée Scrum Master à la joie du partage, en passant par les moments qui grattent, un atelier très agréable.

## Journée spéciale Scrum Masters

  Une journée dédiée aux Scrum Masters, c'est un peu un cadeau pour les intervenants. Des personnes plutôt aguerries dans l'agilité, sensibilisées à l'idée de changement et plutôt motivées pour apprendre, nous avons les conditions rêvées de tout coach ou intervant !
  Par ailleurs, j'avais la chance de pouvoir compter sur l'équipe de coachs, organisatrice de cette journée, qui a offert un cadre optimal et un soutien tout au long de l'atelier pour répondre aux différentes demandes des participants. Ils ont même fait le relai [Tweeter](https://twitter.com/AngGiraud/status/1062300277355753472) :-)

## La joie du partage

De plus en plus, dans les interventions que je fais, que ce soit en atelier ou en conférence, je suis dans une démarche de création de cadre qui permet l'expérimentation. En effet, nous avons tous déjà eu ces moments où nous nous asseyons dans une salle à écouter passivement ce qui est raconté. C'est confortable mais peu utile pour l'apprentissage de nouvelles façons de faire. L'objectif a donc été de faire de cet atelier un moment de partage, mais surtout un moment d'expérimentation. C'est pourquoi les temps d'échanges et d'exercices en groupe sont plus longs que les temps de discours à sens unique.
Petit bonus de cette façon de faire : cela permet des échanges individuels beaucoup plus spécifiques. Là encore, le fait d'être plusieurs coachs, bien que non nécessaire, est un véritable atout pour le groupe.
Les limites : dès qu'il y a expérimentation il y a beaucoup plus de questions, et donc on se retrouve rapidement à courir après le temps.

## Et si on veut faire ça chez nous ?

Vous le savez déjà, cela fait partie de notre philosophie de travail que de partager ce que nous faisons. Ainsi vous trouverez déroulé d'atelier et support utilisé lors de cette journée dans l'article [Feedback Positif](http://ajiro.fr/games/feedbackpositif/). Vous pouvez aussi nous contacter si vous voulez préparer cet atelier et que vous avez des questions. Enfin, si vous avez un public sympa, pas trop loin de là où nous sommes, nous pouvons venir faire un BBL sur le sujet ;-)
