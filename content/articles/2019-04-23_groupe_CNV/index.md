---
date: 2019-04-23
title: "Pas de vacances pour la Communication NonViolente"
draft: true
authors:
  - quille_julie
vie de coach:
  - animation
  - formation
outils:
  - CNV
illustration:
  name: cnv_martinique.jpeg
description: |
 La Girafe Azaé, née à Nanterre, s'est vu le mois dernier voyager en Martinique où nous avons pu proposer d'animer le groupe local encore naissant. Nous profitons donc de ces événements pour vous en dire plus.
---

Vous connaissez déjà notre passion de la Communication NonViolente. Depuis septembre dernier, nous avons l'intention de créer des groupes de pratiques dans l'idée de pouvoir proposer un maximum d'espaces d'échange sur cette pratique que nous affectionnons.
Nous sommes heureux de vous dire aujourd'hui que ces initiatives commencent à prendre forme. En effet, la Girafe Azaé, née à Nanterre, s'est vu le mois dernier voyager en Martinique où nous avons pu proposer d'animer le groupe local encore naissant. Nous profitons donc de ces événements pour vous en dire plus.

## Groupe de pratique

Les groupes de pratique en Communication NonViolente (CNV) ont pour vocations de proposer des lieux de pratique et d'échange autour de la CNV. Les règles de fonctionnement et les thématiques varies d'un groupe à l'autre. Certains sont auto-organisés, d'autre sont créer à l'initiative d'une ou plusieurs personnes qui prendront en charge l'animation du groupe, certains sont payants, d'autres non. Vous l'aurez compris, le seul point commun de ces groupes est la passion de cette pratique que nous a transmis Marshall Rosemberg.

Si vous cherchez un groupe près de chez vous, n'hésitez pas à consulter le site de l'[Association pour la Communication NonViolente (ACNV)](https://cnvfrance.fr/), qui propose une [carte des groupes de pratique](https://cnvfrance.fr/carte-groupes-de-pratique-cnv/).

## Notre expérience de la pratique

En tant que coachs agile chez d'Azaé, nous sommes régulièrement amenés à animer des ateliers autour de la communication, de la gestion de conflit, de la responsabilité individuelle et autres thématiques, au sein des entreprises dans lesquelles nous intervenons. Thalès, EDF, Enedis, Smart-Be, VertBaudet ne sont que quelques unes des entreprises dans lesquelles nous avons eu le plaisir de planter des graines de cette pratique porteuse d'une philosophie d'être qui correspond à notre vision de l'agilité.
En dehors de nos moments intra-entreprises, nous profitons des conférences agiles pour poursuivre ce travail de vulgarisation de la CNV.
Vous trouverez ici notre conférence [La CNV au service de l'agilité](https://www.youtube.com/watch?v=VJnXtoB1RAU), ou encore notre description d'atelier sur [les émotions fortes](https://ajiro.fr/games/emotions_fortes/) que vous pouvez réutiliser dans vos groupe de pratique ou ailleurs.
Aujourd'hui nous souhaitons ouvrir un nouvel espace, accessible à un public différent : nous expérimentons.

## Girafe Azaé - retour d'expérience du groupe de Nanterre

Depuis septembre 2018, nous avons ouvert un [groupe de pratique à Nanterre](). Il nous tient à coeur de pouvoir proposer des espaces de pratique ouverts et gratuits qui viennent en complément des formations et autres moments d'échanges plus formels. L'idée est de pouvoir découvrir ou revoir les bases et surtout de PRA-TI-QUER.
La mise en place d'un groupe est toujours un équilibre subtil : disponibilités, intérêts, différences d'envies ou d'expérience sont autant de points qui construisent la richesse et les faiblesses d'un groupe. C'est donc avec une émotion toute particulière, qu'après un an de travail, la première édition de la Girafe d'Azaé a vu le jour le mardi 12 mars 2019.

Ainsi, pour ceux qui veulent découvrir ce groupe de pratique, et éventuellement construire la suite, nous vous donnons rendez-vous aux dates suivantes :

> Mardi 7 mai 2019

> Mardi 4 juin 2019

## Anecdote : Girafe Azaé en Martinique

Il y a quelques semaines, de passage en Martinique où je rejoignais une de mes collègue de formation CNV, j'ai été invitée à animer le groupe de pratique local. J'ai donc rejoins 5 passionnés pour 3 heures de pratique autour de tisane à la citronnelle et tartes au citron dans un cadre très inspirant. Motivation, expression de gratitude, en vers les autres, mais aussi et surtout envers soi, c'est un beau moment de partage et d'échange auquel je souhaite rendre hommage aujourd'hui. Le plaisir de découvrir des initiatives individuelles et collectives, tout autour du monde, qui, comme dirait Marshall Rosemberg, célèbrent l'élan de vie.
