---
date: 2023-05-11
title: "Quelques pourquois du dire non"
draft: false
authors:
  - quille_julie
themes:
  - dire non
  - CNV

illustration:
  name: no.jpg
description: |
  Une des compétences les plus importantes en entreprise (et dans la vie) est de savoir dire "non". Pourtant on ne vit pas dans la sécurité de pouvoir le dire, le recevoir ou le promouvoir.
Peut-on avoir confiance dans les “oui” si nous n’entendons jamais de “non” ? Savons-nous entendre des “nons” sans vivre un bouleversement interieur ?Avons-nous la confiance que notre “non” peut-être entendu en toute simplicité ? La réponse sont respectivement : non, non et non. Retrouvons-nous pour échanger sur le sujet et pratiquer une nouvelle manière de vivre le “non”.
---

Le sujet du “non” est un sujet qui nous passionne, et une source d'échanges passionnante et infinie. Nous l’abordons ici dans le cadre bien précis de relations consenties d’adulte à adulte en pleine possession de leurs moyens et de leur liberté. Un grand nombre de points ci-dessous seraient d’une violence considérable et à l’opposé de nos croyances si nous sortions de ce cadre précis.

Intro : cet article est le résultat d’une passion partagée, et d’un Meetup animé sur ce sujet au sein de [Cultural Refactorings](https://www.meetup.com/fr-FR/cultural-refactorings/). C’est un mélange de compte-rendu de partages, de précisions ou ajouts de notre part, et d’un désir de partager un point de vue.  Un grand merci à toutes les participantes pour leur participation, leur attention et la richesse de leurs apports.

## Pourquoi dire “non”

On vous parle de dire “non”, et on vous le vend même ! Mais en quoi cela est-il important ?

### Un “oui” qui a du sens :

Tout d’abord, pouvoir dire “non”, c’est se donner la chance de dire de vrais “oui”. Offrir un “non”, c’est nourrir la sécurité de notre interlocuteurice que nos “oui” sont réels.
C’est donner au choix une vraie place, et remettre de la confiance dans la réalité du consentement. Grand sujet que nous ne développerons pas ici malgré notre envie.

### Un “non” qui prend soin :

Dire “non”, c’est également se donner la possibilité d’être juste avec nous-même. Que ce soit respecter nos limites, encourager nos envies ou communiquer nos limites, c’est le potentiel d’être soi avec l’autre, même sans qu’iel soit devin. Il permet également l’apprentissage mutuel de qui on est, je peux faire le cadeau à l’autre de lui partager mon unicité dans mes amours et désamours.

### Un “non” qui informe :

Aussi simple qu’un restaurant qui refuse notre réservation parce qu’il est plein, le “non” nous permet de communiquer le désaccord, l’absence de possibilité, le choix que nous avons fait. Entendons-nous : le fait qu’il soit informatif ne le rend pas pour autant accessible à toustes. Qui d’entre nous n’a jamais accepté un verre qu’iel ne souhaitait pas, ou accepté de faire quelque chose qui n’avait pas forcément beaucoup de sens pour s’épargner l’expression d’un “non” ?

### Un “non” source de possible :

Pour Romeu le “non” est à la vie, ce que le frein est à la voiture : une manière d’aller plus vite, de manière plus sûre. En effet, imaginez une voiture sans freins… Toutes ces précautions à l’approche d’un virage, le sentiment de perte de contrôle si nous devons piloter ce véhicule sans freins dans une pente ou en zone dense : notre plus grande chance de nous en sortir vivant est sans doute le fait de croiser les doigts ! La conduite devient moins enviable tout à coup…
Être en relation sans accès au “non”, c’est un peu la même chose. Pas moyen de s’arrêter rapidement. Nous devenons vulnérables à l’ensemble des éléments extérieurs avec, là encore, le seul fait d’espérer que l’accident n’arrive pas pour survivre.
Dire “non” permet le choix de ce que à quoi on dit “non”. Il serait illusoire de penser que lorsque nous ne disons pas “non” à certaines choses cela veut dire que nous pouvons dire “oui” à tout. N’importe qui ayant déjà été impliqué.e dans des projets pourra sans doute se relier à ce constat.

### Mais encore “non” comme outil de pouvoir

Pour celleux qui y ont accès, nous pouvons nous relier au fait que dire “non” c’est aussi une manière d’expérimenter notre pouvoir. Qu’il soit plus ou moins aligné avec nos valeurs et notre idéal, l’expression de notre pouvoir reste un enjeu de chaque individu.
Un “non” qui me permet de ne pas dire que “je sais pas”, un “non” qui me permet de punir “iel l’avait bien cherché”, un “non” stratégique. Décidément quel pouvoir dans ces trois petites lettres !
 
## Pourquoi il peut être difficile de dire “non”

Pour un grand nombre d’entre nous (nous pourrions être tenté.e.s de mettre des noms sur les catégories de la population qui pourraient être particulièrement concernées, mais nous ne le ferons pas), dire “non” peut-être un challenge ardu, énergivore et souvent peu concluant.

Bien que cela puisse étonner les personnes pour qui le “non” est facilement accessible, les difficultés sont bien nombreuses et réelles.

### Un florilège de peurs
#### Peurs en lien avec l’image de soi

Baigné.e.s dans la croyance qu’une personne aimable se soucie avant tout du bien être collectif (comprendre par là “les autres”), dire “non” peut alimenter la crainte de ne plus être aimé.e, ne plus être aimable, acceptable, de décevoir. Bref devenir une “mauvaise personne” (ou à minima une “moins bonne personne”). De là risque de rejet ou d’exclusion, de mauvaise image de soi.
En contrepartie, dire “oui” permet de nourrir l’estime de soi, la simplicité et même parfois de confirmer que finalement on est peut-être même “une bonne personne”.
Et nous n’en sommes qu’aux effets subtils : “les histoires que je crains que les autres se racontent”, ou encore les micro-agressions. Ce qui nous amène à une catégorie de peurs bien moins subtiles.

#### Peurs des conséquences

De l’idée d’être punie à devoir affronter la colère déchainée de notre interlocuteurice en passant par les conflits divers et variés, les risques de dire “non” ne sont pas uniquement alimentés par des contes pour enfants.

#### Peurs d’absence d’impact

En plus de toutes les peurs de l’impact potentiel, nous avons également les peurs que ce soit tout simplement ignoré, non entendu, non utile. Et quand on fait le bilan :
 - Risque d’impact négatif : ++++
 - Chance d’impact positif : +

Le ratio bénéfice risque est vite fait : bon au final entre faire quelque chose que je n’aimerais pas faire et garder tranquillité ET m’économiser une action que je ne souhaite pas faire au prix de faire face à des remarques désagréables, des conséquences à plus ou moins long terme, et potentiellement ne pas être entendu.e… qu’est-ce que perdre une petite soirée… ou un peu.. beaucoup … plus.
 Pour peu qu’on y ajoute un peu de méconnaissance sur l’existence d’un choix, un peu d’incertitude sur la légitimité de dire “non” et un petit doute de soi, et la recette du “oui” est à son apogée.
Ce qui nous mène à la peur de perdre ce qu’on gagne en disant “oui”.

### Facilités

Dans un système qui encourage le “oui” systématique et en fait le représentant du statu-quo, les bénéfices du “oui” sont non négligeables.
Que ce soit tranquillité, simplicité, confort ou harmonie, on s’y retrouve toustes. L’engrenage du “oui” est bien huilé, et comme pour les peurs, entre simple difficulté à dire “non” et une interdiction violente, il y a un tout un continuum qui permet que le système reste en place. Sans vouloir prendre le temps d’une modélisation systémique qui se voudrait plus ambitieuse que l’intention de cet article, il semble néanmoins utile d’avoir en tête l’absence de hasard dans le statu-quo actuel.

## Un “non” est toujours un “oui” à autre chose

### A quoi je dis “oui” quand je dis “non”

Une proposition pour se connecter différemment au “non” et de chercher à l’intérieur de nous ce qui nous motive à dire “non”. Ce qui revient le plus : prendre soin de son temps et de son énergie, de ses envies, aspirations, prendre soin de soi et de son rythme. Toutes ces choses qui contribuent à l’équilibre de nos vies autant dans le rythme, que dans la joie à ce qu’on fait. Viennent ensuite le sens, la sécurité, la liberté ou encore le soin des autres.
Selon les personnes et les contextes (personnel, professionnel …) ce à quoi on dit “oui” diffère, mais la constance est là : dire “non” à quelque chose, c’est se donner la possibilité de dire “oui” à autre chose. Cette “autre chose” parfois c’est se dire “oui” à soi, vous en conviendrez ça rend l’action de dire “non” d’autant plus essentielle et urgente !

### “Mais ça ne se fait pas de demander ça”

C’est peut-être cette urgence à être en mesure de se donner de la considération, mis en porte à faux avec la complexité de dire “non”, qui fait germer cette phrase dans notre esprit lorsqu’on se retrouve confronté à une proposition face à laquelle nous ne voyons pas d’issue positive pour nous.
Bien loin de blâmer l’apparition de cette phrase : “Ça ne se fait pas de demander ça”, nous posons ici une invitation à y prêter attention en tant qu’un des multiples indicateurs qui nous renseigne sur le fait qu’il se passe quelque chose de désagréable pour nous. Cela est notamment un pilier qui nous permet de passer du “oui impuissant”, ou du “non réactif” pour certain.e.s, au “oui ou non conscient et choisi”.

### Pouvoir exprimer le “oui” derrière le “non”

Exprimer, avec authenticité, le “oui” derrière le “non” est une façon de mettre nuance et clarté sur ce à quoi je dis “non”. Par exemple je peux dire “non” à “un cinéma ce soir”, ce qui implique je ne dis “non” à la proposition et pas à la personne ou à l’intention.
C’est également le premier pas d’une co-construction de proposition alternative. “Non le cinéma ce soir” ne me convient pas, parce que je souhaite partager du temps avec ma fille en bas âge, peut-être qu’une promenade à trois fonctionnerait pour moi. Je me sens fatigué.e et souhaite me reposer ? Peut-être que le même film un autre soir, me tenterait. Je n’ai pas envie d’être enfermé.e ? Peut-être un verre en terrasse serait plus joyeux pour moi. Et de propositions en contre propositions, ce “oui au repos, au partage, au soleil” nous permet également de répondre un “oui” à la relation.

## Passage du “oui impuissant” au “oui conscient”

### Proposition d’expérience

Nous vous proposons de faire un petit exercice en trois temps en lien avec une situation dans laquelle vous avez dit “oui” alors que vous auriez aimé dire “non”.
Vous l’avez ?? Alors c’est parti, avec le maximum d’authenticité que vous pouvez essayer de répondre aux questions suivantes, et vous offrir un vrai temps de réflexion avant de lire les réponses d’autres personnes : 

#### Pourquoi vous avez dit “oui” ?

Des réponses qui sont venues :
- Ne pas décevoir, Faire plaisir, Ménager la personne
- Suivre le groupe, Ne pas déranger
- Devoir, Obligation, Respect de l’autorité

Bien d’autres sont possibles évidemment.

#### Pourquoi vous auriez aimé dire “non” ?

Des réponses qui sont venues : 
- Considération, Préservation, Sécurité
- Prendre soin de son temps
- Liberté
- Estime de soi

Une fois que vous avez pu toucher réellement ces deux premiers éléments, nous vous invitons à l’étape, souvent la plus difficile, qui est également l’étape la plus importante.

#### Quelle est la raison personnelle pour laquelle vous avez dit “oui” ?

Des réponses qui sont venues : 
- Harmonie, Paix, Simplicité, Tranquillité
- Partage, Joie, Plaisir
- Appartenance, Estime de soi, Sécurité

### Ce qui change avec un “oui” conscient

Souvent nous pouvons avoir la crainte que prendre la responsabilité de son “oui”, alors que nous n’en avions pas envie, revienne à la culpabilité, ou à se sentir responsable de tout, avec un sous entendu de “j’ai pas été comme il fallait”.
L’intention ici est inverse. Nous avons la croyance que cette conscience de ce qui nous pousse vraiment à dire “oui”, et de ce qui nous donne envie de dire “non”, nous permet d’être plus outillé.e.s à faire des choix éclairés et justes pour nous.
Par ailleurs, savoir ce qui nous pousse vraiment à dire “oui” alors que nous aurions aimé répondre l’inverse, est une invitation à la compassion pour nous même. Nos peurs mais aussi nos envies et in fine nos valeurs.
Pour finir, même s’il est plus souvent accessible de se dire qu’on a dit “oui” parce que l’autre ne nous a pas laissé le choix, ou “non” parce que l’autre a bien mérité ce “non”, c’est également plus douloureux. Il n’est pas rare que ces croyances nourrissent rancœur, amertume voire colère. Une colère que l’on peut garder avec nous des heures, ou même des jours… Une période au long de laquelle nous avons “la boule au ventre” comme partenaire de vie, alors que notre interlocuteurice de départ n’est pas au courant de ce qui se passe. Réussir à transformer cette situation est avant tout une démarche qui à vocation à prendre soin de soi. Et quand cela n’est pas accessible ? Je me fous la paix, et je me félicite déjà de tout ce que je sais faire !

## Pourquoi est-il important de dire “oui” ?

Nous parlons beaucoup dans cet article de la difficulté à dire “non”. Évidemment tout le monde ne rencontre pas les mêmes difficultés, il y a même des personnes pour qui dire “non” est facile (si, si c’est vrai !). Parfois c’est simplement parce que l’on sait ce qui est juste pour nous, et que nous sommes en capacité de l’exprimer : youpi ça existe !
Il y a aussi d’autres raisons dont les deux dont nous allons parler rapidement ici : le “non” réactif et la “phase exécrable”.

### Les impacts du “non” réactif

En simplifié, le “non” réactif, c’est “dans le doute plutôt dire “non”, au moins je suis assuré.e de ne pas me faire avoir”. Nous pourrions faire tout le cheminement que nous venons de faire sur la difficulté à dire “non”, avec la difficulté à dire “oui”. Les conséquences vont être un peu différentes, les briques de bases également, mais le mécanisme reste le même : ne fonctionner qu’avec le “oui”, ou qu’avec le “non”, bonnet blanc et blanc bonnet : nous nous privons de notre capacité de choix.

### Ma phase exécrable chérie

Il peut arriver qu’après beaucoup d'efforts, potentiellement quelques weekends de développement personnel et quelques expérimentations réussies : eureka, je sais dire “non”, m’occuper de moi ET JE VAIS BIEN ! Convaincu que si tout le monde faisait la même chose le monde serait plus beau, je me sens pleinement responsable de moi, et je laisse à l’autre sa responsabilité. C’est ce que Marshall Rosenberg appelle la “phase exécrable”, qu’on pourrait voir comme une période d’adolescence. C’est une étape nécessaire et qu’il convient d’accueillir avec douceur et amour. L’invitation ici est néanmoins d’essayer de dépasser cette phase pour être en mesure de faire du “ET”. De s’autoriser à être en contact avec les envies de l’autre, notre désir de lui faire plaisir, sans s’oublier. Dire “oui”, c’est aussi une belle manière de vivre l'interdépendance et la joie de l’être ensemble.

## L’essentiel

Comme souvent la première invitation (et celle que nous aimerions que vous reteniez si vous ne devez en retenir qu’une), c’est d’apprendre à mieux se connaître pour augmenter la compassion envers nous-même dans ce que nous faisons déjà. Il peut-être facile d’utiliser tout nouvel apprentissage pour en faire une nouvelle injonction sur nous-même. Vous êtes évidemment libres d’utiliser cette stratégie comme bon vous semble, il nous semble néanmoins qu’elle soit perdant-perdant, perdant pour soi, et perdant pour le monde.

Il y a une multitude de bonnes raisons en lien avec les difficultés à dire “non” (ou dire “oui” pour d’autre), les connaître et savoir qu’elles sont partagées nous permet de favoriser la reconnaissance pour la résilience de notre système.

Derrière chaque “non” il y a un “oui”. Identifier ce “oui” maximise nos chances de mettre en place des stratégies gagnant-gagnant.

Si j’arrive à augmenter ma capacité à dire “non”, j’augmente le nombre de demandes que je peux entendre. Si j’augmente le nombre de demandes que je peux entendre, j’étends ma zone de sécurité et je réduis ma colère. Si je réduis ma colère : je suis plus libre 😀 .
