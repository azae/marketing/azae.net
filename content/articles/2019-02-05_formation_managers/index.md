---
date: 2019-02-05
title: "Formation Agile au Leadership"
draft: false
authors:
  - quille_julie
vie de coach:
  - accompagnement
illustration:
  name: leadership-agile.jpg
description: |
 Depuis quelques mois nous l'attendions, la voilà terminée ! Le 10 janvier dernier, nous sommes partie dans l'aventure d'une demie journée de formation au leadership pour les managers des collectivités territoriales. Une succes story que nous avons plaisir à vous raconter.
---

  Nous sommes régulièrement sollicités pour de nouvelles expériences. Ici, une coopération avec IdealConnaissance, avec l'idée de proposer une formation au leadership à travers des serious game, le tout de manière agile. Une formation sur mesure, avec une optimisation de nos points forts.

## La force du binômage

  Nous entendons régulièrement que le binômage est une perte de temps, pourtant chez Azaé, nous y tenons ! Les raisons sont multiples, nous vous en avons déjà un peu parlé dans notre article sur l'[embarquement d'une équipe](https://azae.net/articles/2018-09-14_atelier_solutionfocus_speedboat/). Ici nous souhaitons essentiellement souligner une des facettes que nous aimons : l'utilisation systématique de l'intelligence collective.
  En effet, nous binômons en permanence, cela crée : de la confiance, de la complicité ainsi qu'une compréhension bien plus effective que ce dont on a l'habitude ailleurs. Sur ce projet, comme sur beaucoup d'autres, le binômage est présent du cadrage jusqu'à la rétro. Le résultat ? Nous cadrons une journée de formation avec le client en très peu de temps, pour une construction de la journée de bout en bout en s'adaptant : au nombre de participants, aux horaires, aux contraintes de communication et d'objectif des différentes parties prenantes. Quelques heures nous auront suffi pour créer un programme de formation sur-mesure à partir de rien, et même un petit temps de création du [teaser de la journée](https://youtu.be/x_F3--dW5I4). Pendant la journée, même chose, nous allions nos compétences et connaissances mutuelles pour nous adapter en temps réel à ce qui se passe. Résultat : un contenu plus riche, des interactions plus personnelles, des partenaires et des participants qui soulignent le plaisir qu'ils ont à travailler avec des personnes qui se complètent. Et bonus non négligeable pour nous : nous donnons l'exemple qu'être deux peut vraiment être une force et un soutien, bien plus qu'une augmentation de la complexité.

## Une formation pour tous les goûts

  Faire une formation sur une demi-journée, c'est jouer contre le temps. Notre objectif : utiliser tous les aspects de la formation pour passer un maximum de contenu.
  Nous nous adressons à un public mixte (c'est quasiment toujours le cas !). Nous devons donc être attentifs à varier les modes d'apprentissage : théorique, expérientiel, kinesthésique, visuel, auditif, rapide, lent. Notre solution : nous en mettons trop, dans toutes les cases pour que chacun puisse repartir avec un maximum d'apprentissages, même si ces derniers sont différents pour chaque participant.
  Nous travaillons sur la forme : programme sous forme de Kanban (que nous ferons évoluer lors du moment de présentation de l'outil), retours d'éxperiences en fin de chaque moment de pratique et ancrage des apprentissages en groupe en fin de demi-journée. Sur une demi-journée, les 20 participants ont vécu l'agilité avec le Kanban, des rétrospectives et de l'intelligence collective.
  A travers les thématiques nous aborderons entre autre l'expression des émotions lors d'un ice-breaker qui permet d'expériementer la Communication NonViolente et de repartir avec un outil de check-In et des cas pratiques de mise en application. [Stylo voyageur](https://ajiro.fr//articles/2018-08-30-stylo-voyageur/), [Moving-Motivators](https://management30.com/practice/moving-motivators/) et bien d'autres élements et experiences avec lesquels les participants pourront repartir dans leurs équipes.

## Une expérience à renouveler

  Suite aux nombreux retours que nous avons reçus il semblait difficile d'arrêter cette expérience à ce stade. Nous voilà donc partis dans le projet de répéter cette expérience plusieurs fois dans l'année avec un public encore plus varié.
