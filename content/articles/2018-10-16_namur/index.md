---
date: 2018-10-16
title: "Voyage au pays <del>des frites</del> de la bière"
draft: false
authors:
  - clavier_thomas
vie de coach:
  - méthode
themes:
  - zemblanité
description: |
  Quel plaisir de découvrir que la SNCF est bien plus agile que la SNCB. Petit chauvinisme teinté d'une projection dans le monde de nos entreprises.
illustration:
  name: sncb.jpeg
  credit: https://twitter.com/jihan65
---

Habitant Lille, je vais régulièrement en Belgique, très souvent en courant, de temps en temps en voiture et rarement en train. Ce matin là, j'ai choisi cette dernière option pour rejoindre Namur.

## Le drame

Pour rallier Namur depuis Lille en train, il faut prendre le TGV de Lille à Bruxelles puis un train genre corail entre Bruxelles et Namur.

Il faut compter 35 minutes entre Lille et Bruxelles ... Ce matin, suite à un vol de câble, nous avons mis plus d'une heure.
Arrivé en gare de Bruxelles-Midi j'avais raté mon train pour Namur et j'ai observé avec fascination l'organisation de la gare.

## Tout est planifié

Je n'avais jamais été frappé par ce point, en gare de Bruxelles-Midi tous les trains sont affichés avec leur quai, de longues heures à l'avance, avec en prime l'heure de début d'embarquement. Quand tout fonctionne bien, c'est très pratique, en tant que client, pas besoin d'attendre debout devant le panneau d'affichage pour connaitre son quai de départ.

Quand il y a un problème, et c'était le cas ce matin-là, ça tourne à la catastrophe.

Un train en retard entraine des changements de voies, il faut donc prévenir et annoncer le changement de promesse, <cite> « Le train en direction de Namur ne partira pas quai 14 mais quai 17 avec 7 minutes de retard, l'embarquement se fera de 8h29 à 8h34 » </cite> en français et en flamand, hautparleur et guichet sont fortement mis à contribution. Les clients sont perdus, saturent les points d'informations et bon nombre ratent leur train, ce qui les conduit à pester auprès des guichets. De cette pagaille, d'autres trains vont se trouver retardés.

Les horaires d'embarquement sont aussi planifiés à l'avance. On peut observer des trains à quai toutes portes fermées, incapables de partir.
Les clients en retard sont frustrés de ne pas pouvoir profiter du train, et les nombreux clients du train suivant, n'ayant pas repéré que c'est encore l'ancien train, sont furieux de ne pas pouvoir monter à bord.
En effet, l'embarquement du train suivant sur le même quai est annoncé comme ayant commencé.

## Responding to change over following a plan

La Radio Télévision Belge Francophone (RTBF) titrait l'autre jour : “3 trains sur 10 sont à l’heure sur la ligne Namur - Bruxelles” voilà qui illustre l’ampleur du problème.

Alors que la SNCF est consciente que le fonctionnement normal consiste à faire face aux aléas de la vie, la SNCB semble persuadée que le fonctionnement normal c’est quand il n’y a pas de problème. Cependant la question se pose, même si la SNCB décidait aujourd'hui de modifier son fonctionnement, quel serait l'impact sur ses clients ?
Intrigué par cette situation, j'ai profité de cette expérience pour discuter avec les belges que je venais voir. Leur première réaction a été : <cite> « Ah oui, c'est la galère ces trains en retard ! » </cite> rapidement suivi de <cite> « Ah non, le système Français est insupportable ! » </cite>

Dans un système complexe comme celui là, qui doit changer en premier : la SNCB ou les clients ? Faut-il se concentrer sur la réduction des retards, n'annoncer que des promesses tenables ou améliorer les moyens d'informer les voyageurs en cas de changement ? Et en cas de changement comment est-il possible de ménager les habitués et leurs habitudes ? Une histoire dans laquelle nous voyons que l'accueil du changement concerne toutes les composantes d'un système.
