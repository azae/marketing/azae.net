---
date: 2023-07-10
title: "Argent et mérite : une histoire de déconstruction, d'innovation et de collectif"
draft: false
authors:
  - quille_julie
themes:
  - entreprise
  - gouvernance
  - rémunération
  - argent

illustration:
  name: argent1.jpg
description: |
  Chaque séminaire est l'occasion d'expérimenter, de grandir, d'apprendre. Durant notre séminaire de juin nous avons pu mettre en oeuvre des manières de répartir de l'argent en réduisant autant que possible le lien avec une idée de mérite. Retour d'expérience sur ce que nous avons fait, et ce qui en est ressorti.
---

## Une histoire inscrite dans un mouvement

Les sujets de justice sociale et d’entreprise au service de l’empouvoirement des individus est dans l’ADN même d’Azaé.

Depuis sa création, ou presque, Azaé s’inscrit dans le [Réseau Libre Entreprise](https://www.libre-entreprise.org/), réseau qui promeut la philosophie du logiciel libre, autant à travers les outils que dans les modèles organisationnels (à ce sujet je vous invite à visionner [la conférence de Jean Couteau : C'est qui le patron ?](https://mixitconf.org/2023/c-est-qui-le-patron-)). Un réseau constitué de petites et moyennes entreprises qui se questionnent sur la répartition du capital, que ce soit à travers la possession de l’outil de travail que représente l’entreprise, ou la manière de distribuer l’argent à travers les revenus. Le salaire unique (indépendamment du poste, de l’ancienneté, et même parfois du temps de travail), est monnaie courante.

En 2017, nous étions déjà dans une dynamique de démocratisation de ces modèles à travers des articles, tel que celui sur [La rémunération libre](http://ajiro.fr/articles/2017-01-26-remuneration\_libre/), et conférences ([La rémunération entreprise Agile ou libérée](https://youtu.be/GweO\_uJ1rI8)), sur le sujet pour montrer que d’autres manières de faire étaient possibles. Néanmoins, la question de la répartition de l’argent au sein d’Azaé est un sujet qui revient sans cesse, parce que nous aimons l’améliorer, mais également parce qu’il est souvent, de manière inconsciente la plupart du temps, une stratégie pour recevoir reconnaissance, valeur, amour et justice au sein de l’entreprise. 

Il est d’ailleurs intéressant de se poser la question de ce qui nous motive quand nous cherchons à gagner plus ? Est-ce un réel besoin d’augmenter notre rythme de vie, ou une question de justice vis-à-vis du marché ou d’un.e collègue qui gagne plus ? Une manière de se rassurer sur le pourquoi d’un emploi du temps pénible que nous maintenons essentiellement pour l’apport financier qu’il nous apporte ? Une façon de se rassurer sur la manière dont notre entourage, professionnel, personnel et/ou sociétal nous perçoit ?

De multiples expériences ont donc été faites dans Azaé, beaucoup ont échoué, certaines ont fonctionné. Toutes nous ont appris, sur le réalisme de nos hypothèses, mais aussi sur nous-mêmes. Et toutes nous ont permis d’arriver au modèle actuel, qui a évidemment vocation à évoluer, mais qui nous permet aujourd’hui d’être fièr.e.s de la manière dont nous incarnons nos valeurs.

## Une mise en pratique intégrée dans la structure

Nous vous partagions le mois dernier la manière dont nous arrivions à faire [plus de commun au niveau financier dans Azaé](https://azae.net/articles/2023-06-22\_histoire\_raison\_etre/). Notre dernier séminaire nous a permis d’expérimenter, pour la première fois de manière concrète, une façon de vivre l'argent dans l'entreprise comme stratégie de soutien de chacun.e et de nos valeurs.

### Qu’est-ce que le PAP d’Azaé

Le PAP est le Pot à Paillettes, une caisse dans laquelle toutes les personnes facturant à travers Azaé cotisent pour contribuer à changer le monde, et notamment notre rapport à l’argent et au mérite. Le PAP représente aujourd’hui 5% du chiffre d’affaire interne d’Azaé. Il a vocation à alimenter deux stratégies de répartition d’argent innovantes : le PAP interne (50% du PAP) est une somme que nous répartissons en interne de manière non déterministe (nous ne savons pas à l’avance qui va toucher quoi), le PAP externe (50% du PAP) est une somme que nous choisissons de donner à des structures, associations, causes qui nous tiennent à coeur, qui nous semblent contribuer au changement social que nous souhaitons soutenir.

Lors de notre séminaire c’est près de 13 000€ que nous avons reparti. Cette fois-ci nous avons choisi d’expérimenter la répartition en suivant la logique du [Tas d’argent de Dominic Barter](https://cerclesrestauratifs.org/wiki/Coresponsabilit%C3%A9_financi%C3%A8re).



### Répartition du PAP interne

Souhaitons-nous des revenus intégralement indexé sur la rémunération ? Quels liens souhaitons-nous avoir entre congé menstruel et notre fonctionnement ? Comment créer autre chose qu'une rémunération basée sur une idée de mérite ? Autant de questions, et bien d'autre, qui nous animent et sur lesquelles nous souhaitons expérimenter dans le concret de nos rémunération. Nous avons donc choisi de mettre en commun un montant qui est assez conséquent pour que nous puissions avoir un impact sur nos revenus, et donc avoir des discussions de fonds. Mais également suffisamment petit pour garder la sécurité suffisante pour que sortie de zone de confort ne soit pas équivalent à être en danger. C'est la raison d'être du PAP interne (2,5% de notre facturation).



Pour le PAP interne, nous nous sommes mis.e.s autour de la table et nous avons matérialisé, au centre de la table, la somme d’argent à répartir. Nous avons ensuite  suivi le déroulé suivant :

   * Tour d’expression : y a-t-il quelque chose qui m’empêche d’entrer pleinement dans le processus (par exemple conflit avec une autre personne, tension interne etc.) ?
   * Tour d’expression : où en suis-je aujourd’hui dans mes besoins financiers, et potentiellement mon rapport à l’argent ?
   * Temps de répartition : chaque membre peut, à n'importe quel moment, prendre de l’argent (de n’importe où, de la pile au centre ou dans le tas existant d’une personne), et le déplace en le mettant devant une personne (elle-même incluse). Dans la mesure du possible ce geste est accompagné d’une explication sur ce qui le motive.
   * Quand les tas se stabilisent nous arrêtons la répartition.
   * Tour d’expression : comment nous avons vécu l’expérience, et acceptons-nous le résultat ?


Durant tout le processus les personnes participantes sont invitées à mettre de l’énergie dans la conscientisation de ce qui se passe pour elles et l’exprimer si c’est possible.

Quelques pépites, en vrac, de ce que nous retirons de l’expérience :

   * Dans les craintes : craintes de ne pas « oser », craintes de l’état émotionnel des autres.
   * Dans les motivations à mettre de l’argent quelque part : soutien vis-à-vis de prestations non payées, acte de reconnaissance d’un apprentissage fait à travers une personne, soutien à un projet futur de pouvoir faire un break, soutien par rapport à une décision passée de prendre du temps d’une manière inspirante pour le groupe, envie de justesse dans la répartition etc.
   * Dans les expériences vécues : difficulté à ne pas se comparer en tant qu’individu en lien avec qui recevait combien, difficulté à ne pas associer l’argent avec expression d’amour, et bien d’autres, reconnaissance pour des personnes qui n’ont pas cotisé au PAP de repartir avec une partie de ce dernier.
 
### Répartition du PAP externe

C’est environ 5 800€ que nous souhaitions répartir pour des causes qui rentrent dans la démarche sociale et politique d’Azaé.

Pour ce faire nous avons suivi un processus inspiré de ce que nous avons fait pour le PAP interne, à ceci près que nous répartissions entre des causes et non pas des personnes. Une difficulté qu’il a été important de pouvoir dépasser pour faire cet exercice est de déconstruire le lien entre argent donné et hiérarchisation des causes (de la même manière qu’il ne s’agissait pas de hiérarchisation des personnes dans l’exercice précédent). 

Nous avons fait le choix de commencer par identifier les causes que nous souhaitions soutenir avec la décision de répartir le travail d’identification des acteurices du domaine (organismes, associations) dans un second temps.

Bien que sans aucun doute imparfait, nous sommes heureuxses d’avoir pu participer à répartir de l’argent entre ces six causes :

Soutien à la contestation / Féminisme / Anti-racisme / Lutte contre la précarité / Logiciel Libre / Climat

Cette démarche est aussi l'occasion pour nous de nous rapprocher d'autres acteurices engagé.e.s tel que [Copie Publique](https://copiepublique.fr/) dont les membres reversent un pourcentage de leur CA pour soutenir le Logiciel Libre.

Nous entrons maintenant dans une phase de recherche et d’information sur les associations que nous souhaitons soutenir en tant que collectif. Nous aurons sans doute le plaisir de vous partager d’ici quelques semaines les choix que nous avons fait. Une chose est certaine, nous avons pu constater une fois de plus que nous nous retrouvions toustes autour d’un souhait fort d’engagement concret dans nos valeurs.

## Croyances et système de valeurs

Le travail, l’argent, l’effort et le mérite sont souvent entremêlés dans un système complexe rempli d’affects, de réflexes, de peurs et de croyances diverses. Entrer dans une démarche qui soutient le « pouvoir avec » et qui tente, autant que faire se peut, de lutter contre les systèmes de domination nous invite à revoir notre rapport individuel et collectif à toutes ces notions et notamment à cette démarche, souvent internalisée, de hiérarchisation. Hiérarchisation de la valeur, de l’effort, des besoins, des bonheurs et des peines, des sentiments et in fine, de manière plus ou moins assumée, hiérarchisation des personnes.

Ce travail de prise de conscience et d’alignement autour de l’envie de faire différemment est un tel travail qu’il peut paraitre l’essentiel du chemin. Il nous semblera alors trivial, un fois ce constat partagé, de créer des systèmes qui soutiennent autre chose. Néanmoins les questions autour de ce sujet sont aussi passionnantes qu’infinies. Et c’est sans doute un travail, qui une fois l’alignement atteint, se jouera sur plusieurs décennies, voire plusieurs générations.

Ce sujet nous tenant à coeur, c'est la raison pour laquelle nous cherchons à apporter notre pierre à l’édifice que ce soit à travers la création de nouveaux modèles, ou dans la création d’espaces de déconstruction pour les individus. Nous poursuivons donc nos expérimentations et nous partageons, avec autant de transparence que possible nos apprentissages. 

Une grande gratitude à mes camarades de voyage, ensemble nous allons plus loin, mais actuellement nous allons également plus vite !

## Quelques ressources pour creuser le sujet :

Livres :
* [Tyrannie de la norme - Todd Rose](https://www.placedeslibraires.fr/livre/9782266283755-la-tyrannie-de-la-norme-todd-rose/)
* [La juste part - David Robichaud, Patrick Turnel](https://www.placedeslibraires.fr/listeliv.php?base=paper&mots_recherche=la+juste+part)

Vidéos :
* [Rémunération dans une entreprise Agile ou Libérée - Olivier Albiez - Julie Quillé](https://youtu.be/GweO_uJ1rI8)
* [C'est qui le patron - Jean Couteau](https://mixitconf.org/2023/c-est-qui-le-patron-)
* [Amargi (pièce de théatre) - Judith Bernard](https://youtu.be/71TXQQVpG0Q)

Associations :
* [Mouvement Français pour un Revenu de Base](https://www.revenudebase.info/)
* [Copie Publique](https://copiepublique.fr/)
