---
date: 2020-02-25
title: "Agi'Lille 2020"
draft: false
authors:
  - clavier_thomas
vie d'orateur:
  - Agile Tour Lille
  - Agi'Lille
illustration:
  name: lille.jpg
  description: beffroi de la cci
description: |
    Pour inogurer le nouveau nom de l'Agile Tour Lille, nous avons proposé tout plein de sujets.
    Voici un rapide tour d'horizon de nos propositions.
---

Cette année encore, nous avons proposé un large choix de sujets. Comme les années précédente nous avons donné un grand nombre de conférences à l'Agile Tour Lille, ce ne sont quasiment que des nouveaux sujets, des sujets qui nous anime en ce moment.
Voici un rapide tour d'horizon de nos propositions.

## Changement de nom

Une des conditions pour être [Agile Tour](https://agiletour.org) c'est "A date for the event between October and December" or les organisateurs lillois ont changé la date pour fin juin, en même temps qu'[agile Laval](http://www.agilelaval.org).

En janvier, les membres de [Nord Agile](http://nord-agile.org) se sont [regroupés pour choisir un nouveau nom](https://twitter.com/NordAgile/status/1223005748508139527).
L'ambiance était bonne, des noms plus ou moins originaux ont vu le jour comme "Welsh Tour", "Agilichti" ou "L'agile À Gilles" et c'est finalement [Agi'Lille](https://twitter.com/hashtag/AgiLille) qui a remporté la majorité des votes.

Cette édition 2020 voit donc deux changements majeurs :
- Le changement de date : les 25 et 26 juin 2020.
- Le changement de nom, ce n'est plus "Agile Tour Lille" mais "AgiLille"

En corolaire, il y aussi le changement de site, ce n'est plus [agiletour-lille.org](http://www.agiletour-lille.org) mais [agilille.fr](https://agilille.fr/)

## Nos propositions

Comme l'année dernière l'appel à orateur, call for papers en anglais (CFP) se passe sur [sessionize](https://sessionize.com/agilille-2020), bonne nouvelle, je peux réutiliser mes identifiants.

Toutes nos conférences sont visibles en ligne sur le site d'[ajiro](https://ajiro.fr//talks/) pour cette année j'ai choisis : 

* [Vivre le Behavior Driven Development (BDD)](https://ajiro.fr/talks/atelier-bdd/), un atelier de 2h pour expérimenter le BDD sur un cas simple.
* [Host leadership, la métaphore de l’hôte au service du leadership](https://ajiro.fr/talks/host-leadership/), une conférence atelier qui a eu un franc succès à Nantes et Laval
* [Programmation multi-agents pour les nuls], un atelier ludique dans lequel nous simulerons un programme muti-agents avec des post-it et des feutres.

