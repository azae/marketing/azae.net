---
date: 2019-01-02
title: "Meilleurs vœux pour 2019"
draft : false
authors:
  - albiez_olivier
  - clavier_thomas
  - benoist_alexis
  - quille_julie
vie d'Azae:
  - interne
description: |
  Pour cette première publication de l'année nous vous souhaitions une excellente année 2019. De notre côté, nous gardons les mêmes passions : partage, découverte, amélioration, et nous allons continuer de les partager avec vous.
illustration:
  name: bonne-annee.jpg
---

Merveilleuse année 2019 à tous !

Nous vous souhaitons de vivre une année pleine d'apprentissages, de réussites et d'agilité.
Nous vous souhaitons de profiter pleinement de chaque instant de la vie et d'en extraire ce qui vous enrichi le plus.

De notre côté nous reprenons l'année avec passions et plaisir, avec une équipe toujours aussi extraordinaire ! Et qui sais peut-être que demain, vous aussi vous ferez parti de cette merveilleuse équipe.
Nous espérons avoir de nombreux sujets à partager avec vous ici.
Nous attendons avec impatience les nombreux échanges que nous allons avoir ensemble et nous vous remercions d'avance pour leurs richesses.
