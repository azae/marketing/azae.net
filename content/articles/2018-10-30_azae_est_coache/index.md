---
date: 2018-10-30
title: "Les coachs coachés"
draft: false
authors:
  - quille_julie
vie d'Azae:
  - interne
vie de coach:
  - accompagnement
illustration:
  name: equipe_azae.jpeg
description: |
 Mardi dernier, c'est Azaé dans son entier qui s'est arrêté une journée complète pour se faire coacher. Travail sur la dynamique à l'intérieur de l'équipe, travail ensemble, confrontation, outils, rémunération... vous l'aurez compris une journée riche en cogitations et en émotions pour tous. Nous vous parlons ici de pourquoi nous l'avons fait et de ce que cela nous a apporté.
---

En tant que coachs, agile, craft ou autre, nous ne cessons de répeter, aux clients que nous accompagnons, l'importance du regard extérieur et de la prise de recul. Conscients que nous ne faisons pas exception à la règle, nous avons prévu une journée pour être coachés. En fin de journée nous pouvons dire que l'objectif a été atteint : nous avons pris du recul, nous avons eu des apprentissages, nous avons avancé sur des sujets qui nous tenaient à coeur. Merci à notre coach.

## Les coachs aussi ont besoin d'être coachés

"Quel que soit votre niveau de maturité, vous pouvez toujours vous améliorer.", "Quand on est en permanence dans un environnement, on devient aveugle à ses faiblesses." Voilà des phrases qu'en tant que coachs nous ne cessons jamais de répéter. C'est la raison pour laquelle, malgré notre malette à trucs et astuces pour s'améliorer, prendre du temps et décider ensemble, nous nous sommes dit que nous ne pouvions pas faire exception à la règle : nous aussi, nous pouvons tirer des bénéfices du fait de nous faire coacher. Et c'est ce que nous avons fait. Alignés sur notre envie de trouver des solutions ensemble et d'attaquer nos grains de sable plus ou moins gros, nous avons fait appel à un de nos amis d'[/UT7](https://ut7.fr/), Raphaël, pour venir nous accompagner sur une journée que nous n'avions dédiée qu'à ça.

## Une prise de recul indispensable pour vérifier que nous avons bien les ressources nécessaires

Pris dans notre quotidien et nos habitudes, nous avons du mal à prendre du recul sur notre pratique de manière significative. C'est à travers un espace temps réservé et sécurisé que nous avons pu faire un bout de chemin. Un début un peu tâtonnant, on se cherche, on se regarde, on tâte le terrain : où se situe notre trou dans la raquette, nous ne le savons pas, enfin pas vraiment... notre coach non plus ! Garant du cadre et du rythme, Raphaël ne nous apporte pas les solutions, il nous questionne, nous fait quelques retours d'expérience, vient en soutien à l'expression de chacun. Des petites choses, presque imperceptibles mais en moins d'une heure nous voici à expérimenter un nouvel outil pour nous aider à nous confronter et à aller au bout de sujets qui trainent dans le fond de notre backlog personnel depuis des mois. Cela procure un sentiment assez fantastique de se sentir avancer, tout en ayant l'impression que nous pourrons le refaire seuls, ce qui est, après tout, l'objectif final ; nous sommes en train de grandir.

## Un coup d'accélérateur dans notre courbe d'apprentissage

Petit [ROTI](http://www.atelier-collaboratif.com/50-le-roti-agile.html) de fin de matinée, des 5 sortent, Raphaël plaisante sur le fait que nous pouvons donc nous arrêter là car nous avons déjà gagné notre journée, nous rigolons, nous réflechissons ... et nous nous disons qu'en effet, même si nous n'avions que les apprentissages de la matinée, nous aurions gagné notre journée ! Mais malgré le gâteau, nous restons preneurs de la cerise ; on continue jusqu'à la fin de la journée en mode intensif.
Nos apprentissages sont multiples :
- Nous apprenons des outils, des cadres, que nous pouvons mettre en place pour faciliter nos échanges. Nous co-créons les outils, ils n'ont pas de nom, nous les utilisons quand même.
- Nous apprenons à nous connaitre mieux, savoir ce qui nous motive, là où nous nous séparons et là où nous nous rejoignons, nos supers pouvoirs respectifs auxquels nous allons pouvoir faire appel, nos petits défauts qui nous rendent plus forts tous ensemble.
- Nous prenons conscience de certaines dynamiques sous-jacentes, des points sur lesquels nous pourrons être vigilants à l'avenir, des endroits où nous souhaitons mettre une attention particulière.

## Coach un jour coach toujours : l'expérience du changement de posture

"Aujourd'hui nous sommes coachés... mais nous restons coachs à l'intérieur". Cette expérience a une dimension toute particulière chez Azaé puisque même coachés nous restons coachs. Une occasion magnfique pour nous de voir une manière différente de coacher, avec des points communs, mais surtout, le plus intéressant, des différences ! Nous prenons un plaisir à l'expérimentation et nous nous délectons des moments de méta, ces moments où nous montons d'un cran pour parler de ce qui vient de se passer... mais aussi des moments de méta sur le méta, les moments où nous montons d'un cran pour analyser comment nous avons pris du recul sur ce qui venait de se passer... mais aussi des moments de méta du méta du méta, c'est à dire... enfin vous l'aurez compris nous aimons ça.
Et nous gardons le meilleur pour la fin. Cette journée nous permet aussi de nous remettre dans la peau d'une personne à l'intérieur d'une équipe. Avec les difficultés que nous rencontrons, les changements que nous voudrions faire mais que nous n'arrivons pas à finaliser, les agacements qui nous font perdre notre accueil inconditionnel de l'autre, et notre quotidien qui prend le dessus avec  ces urgences que nous connaissons si bien dans les équipes que nous accompagnons. Nous sommes ressortis de cette journée grisés et épuisés de ce trop plein d'émotions, d'introspection, de remise en question, avec un réservoir d'empathie pour les équipes et les personnes que nous accompagnons au quotidien, régonflé à bloc.
