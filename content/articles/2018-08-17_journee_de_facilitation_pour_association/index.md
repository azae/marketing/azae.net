---
date: 2018-08-17
title: "Journée de facilitation"
draft: false
authors:
  - quille_julie
vie d'orateur:
  - MFRB
vie de coach:
  - facilitation
  - médiation
illustration:
  name: 2018-08-16_Camp-de-base.png
description: |
  Une journée de facilitation en plein milieu de l'été dans la campagne française, avec un groupe de personnes qui se réunit régulièrement dans un objectif de faire avancer la société grâce à l'intelligence collective.
---

# Le milieu associatif et militant

  Il y a un peu plus d'un an j'ai découvert le [MFRB](https://www.revenudebase.info/) (Mouvement Français pour un Revenu de Base). J'ai été passionnée par les thématiques abordées : revenus, travail, liberté, peurs, par le format des échanges : intelligence collective, sociocratie, bénévolat, par l'intelligence et la qualité de l'ensemble des échanges que j'ai pu avoir. Investie dans le mouvement depuis un an, ce sont les sujets de réflexion autant que le mode d'organisation qui m'interpellent et me passionnent encore aujourd'hui.

# Le Revenu de Base et l'agilité ? Quel rapport ?

<img src="atelier.jpg" class="img-fluid" alt="Un atelier auto-organisé par les membres du MFRB"/>


  Chez [Azaé](https://azae.net), comme dans de nombreuses autres entreprises, le revenu fait partie intégrante de nos sujets de préoccupation.

> Le rapport à l’argent est un fort vecteur de positionnement individuel et collectif, il modèle notre rapport à l’autre, au travail et à l’entreprise. Dans cet article, nous allons vous montrer comment, en laissant les salariés choisir leur rémunération, nous changeons en profondeur l’ADN de l’entreprise et des individus qui la composent.
[Extrait de l'article sur la Rémunération d'InfoQ](https://www.infoq.com/fr/articles/la-remuneration-libre)

  Le MFRB poursuit la même logique au niveau sociétal. Du coup il n'est pas très étonnant de retrouver parmi les membres actifs de nombreux adeptes de l'agilité, et de l'intelligence collective en général. D'ailleurs le modèle de gouvernance du mouvement est sociocratique !

# Facilitation d'une journée de travail sur la gouvernance

<img src="regles_paroles.jpg" class="img-fluid" alt="Quelques règles sur la prise de parole affichées au mur permettent de faciliter la gestion du cadre."/>

  En plein milieu de ce mois d'août, j'ai été invitée par le MFRB à aller faciliter une de leurs journées de travail sur la gouvernance. La facilitation est un de mes outils privilégié dans les entreprises, j'ai donc pris un grand plaisir à aller faciliter cette journée passionnante.
  Quelques petites choses qui ont été mises en place pour structurer cette journée et qui peuvent être réutilisées sans modération :
  - afficher le déroulé de la journée (donner de la visibilité sur ce qui va se passer)
  - afficher quelques règles de prise de parole (d'une manière générale, afficher les principes clés facilite grandement le maintien d'un cadre sécurisant)
  - vérifier et préciser les intentions des organisateurs pour chacun des moments (l'intention est de clarifier l'objectif de chaque moment ainsi que ce à quoi il répond. Cela permet entre autre de savoir si nous sommes dans la bonne direction ou non, ainsi que de savoir quand un sujet est terminé)

Un très [bel événement](https://www.revenudebase.info/evenement-ete/) auquel j'ai eu plaisir de participer et une grande joie d'avoir pu contribuer au bon déroulé de celui-ci.
