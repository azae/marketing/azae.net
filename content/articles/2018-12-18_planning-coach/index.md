---
date: 2018-12-18
title: "Planning de coach"
draft: false
authors:
  - quille_julie
vie de coach:
  - facilitation
  - méthode
outils:
  - management visuel
illustration:
  name: planning-azae.jpg
description: |
  En tant que coachs agiles, nous avons toujours, parmi nos objectifs, le fait de nous rendre dispensable. Apporter du changement, de l'expertise, de l'accompagnement tout en augmentant l'autonomie, la souveraineté et l'auto-organisation des équipes est un défi auquel nous faisons face au quotidien. Dans cet article nous vous partageons notre dernière exprimentation en date pour favoriser cet équilibre subtil.
---

## Management visuel pour tous

Une des premières actions que nous avons menée chez notre client a été de mettre en place du management visuel pour tout le monde. Nous avons travaillé les classiques : identité et spécificité de l'équipe, kanban de l'encours, radiateur d'information en tout genre.
Et bien nous n'avons pas fait exception à la règle. Bien que présents en moyenne un jour par semaine, nous avons estimé que, pour la durée de notre intervention, nous faisions partie des acteurs du service. Nous avons affiché qui nous étions, nos contacts, notre raison d'être pour cette mission, les acteurs auprès desquels nous intervenions, les services que nous proposions. L'objectif de cette étape : nous rendre accessible aussi bien par nos coordonnées qu'en rendant visible ce pour quoi il était possible de faire appel à nous.
Un autre élément qui a été intéressant pour nous fut d'afficher nos jours de présence et notre planning sur ces journées. L'idée étant que chacun puisse voir où nous sommes et ce que nous faisons.

## Visibilité pour favoriser l'autonomie

<img src="identite-azae.jpg" class="img-fluid" alt="Nous nous présentons"/>

Souhaitant que, le plus rapidement possible, les équipes soient en mesure de prendre en main leur apprentissage, identifier leur besoin et faire appel à nous quand nécessaire, nous souhaitions leur donner les moyens de le faire. Bien que nous sachions que cet apprentissage prend du temps, nous savons aussi que l'accessibilité à l'information et aux personnes est un élément important de facilitation de prise d'autonomie.
Nous avons présenté notre planning à chacune des équipes que nous accompagnons et leur avons expliqué le fonctionnement : et il est simple :

 - un planning accessible à tous
 - des créneaux "occupés" : ateliers ou autre, auxquels ils peuvent se joindre
 - des créneaux libres dans lesquels ils peuvent programmer ce qu'ils souhaitent
 - un moyen d'aller discuter avec les autres équipes pour négocier les priorités si besoin

## Nos succès

Nous avons eu le plaisir de voir très vite que les personnes que nous avons l'habitude d'accompagner mettent d'elles-mêmes le planning à jour :

- préparation de rétro de telle à telle heure, suivi de la rétro : c'est dans le planning, nous savons que nous y serons
- et vis-versa, nous avons vu une planification de rétro se programmer en fonction de nos jours de présence, ce qui est possible car visible
- un atelier proposé à une équipe, ouvert à l'ensemble des équipes que nous accompagnons
- une initiative personnelle de promouvoir les valeurs de l'agilité, pour laquelle nous sommes sollicités pour préparer l'atelier, et assister à son déroulement

## Nos pistes d'évolution

Comme tout management visuel, nous espèrons bien que celui-ci va évoluer avec le temps. Nos axes de réflexions actuels sont :

- comment rendre visible notre action d'une manière plus globale (attentes, démarche etc.)
- comment équilibrer nos interventions et ne pas faire uniquement du "premier arrivé, premier servi"
- comment nous assurer d'une bonne communication au-delà de ce qui est affiché (notamment pour les ateliers)
- comment garder l'émulation des débuts tout au long de l'accompagnement

## Premier bilan

Pour l'instant, nous n'avons encore que peu de recul sur cette expérience. Il n'en reste pas moins que nous avons déjà pu en mesurer les bénéfices et que nous apprécions la démarche qui allie fond et forme dans notre manière d'intervenir chez nos clients.
Un autre point qui facilite grandement notre intervention est que nous avons sur place des alliées de taille qui mettent une très belle énergie pour accompagner le changement avec beaucoup d'attention et de bienveillance. Nous ne saurions sous estimer l'impact de cela dans l'implémentation d'expériences liées à la transformation.
Nous ne manquerons pas de vous partager la fin de l'histoire : rendez-vous en fin de mission.
