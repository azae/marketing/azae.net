---
date: 2018-06-15
title: "Agile France 2018, nous y étions !"
draft: false
authors:
  - quille_julie
vie d'orateur:
 - Agile France
themes:
  - zemblanité
outils:
  - groupe de paroles
illustration:
  name: agilefrance2018.png
description: |
    La boucle est bouclée, me voici à la fin de ma première année de conférences Agile à travers la France. Mais comme à Azaé nous fonctionnons par itération, c’est aussi un début puisque ce fut la première conférence Agile d’Alexis, notre dernier arrivé chez Azaé.
---

La boucle est bouclée, me voici à la fin de ma première année de conférences Agile à travers la France. Mais comme à Azaé nous fonctionnons par itération, c’est aussi un début puisque ce fut la première conférence Agile d’[Alexis](/team/), notre dernier arrivé chez Azaé.

[Agile France](http://2018.conf.agile-france.org/) qu’est-ce que c’est ? C’est une cinquantaine de conférences, et encore plus de speakers, sur l’agilité au sens large… très très large ! Un cadre magnifique (oui, oui ça compte aussi). Et surtout pour nous l’occasion de retrouver nos amis de l’agilité de toute la France.


## Qu’est-ce que nous y avons fait ?

<img src="zemblanite-thomas-olivier.jpg" class="img-fluid" alt="Thomas et Olivier parlent de zemblanité"/>

Pour moi ce fut l’occasion d’expérimenter un nouveau sujet, un nouveau format, un nouveau binôme, enfin de la nouveauté. En effet, j’ai co-animé l’atelier “La parole au travail : un espace de privilège masculin ?” avec [/ut7](https://ut7.fr/). Nous ne nous limitons plus à parler de l’importance des émotions et de leur place : nous les vivons ! Mais je ne vous en dis pas plus, un autre article plus spécifique y sera consacré.
Azaé ne s’est pas arrété là dans sa présence oratoire à cette édition d’Agile France 2018, puisque [Olivier](/team/) et [Thomas](/team/) ont pu partager leurs connaissances approfondies sur la zemblanité et sa mise en pratique, aux auditeurs les plus curieux. Vous ne savez pas encore ce qu’est la zemblanité ? Pas de soucis, tout est [ici](http://ajiro.fr/talks/zemblanite/) !


## Les coups de coeurs :

Le petit plus d’Agile France, ce sont les sourires qui s’échangent tout au long de ces deux journées. Je ne parle pas seulement des visages joyeux que nous rencontrons en haut de corps pressés de passer d’une salle à l’autre pour découvrir de nouvelles conférences, mais aussi de ces carrés de papier sur lesquels vous pouvez écrire en quoi une personne vous a enrichie et l’offrir à la personne concernée.

Deuxième coup de coeur pour cette conférence en plein centre parisien : le cadre. Des chalets, des salles (parfois un peu petites, mais ça fait partie du charme), des espaces dehors dont on profite grandement quand le soleil est au rendez-vous, des arbres, de l’eau, des petits-déjeuners, déjeuners et pauses café riches en qualité et en quantité. Si nous gardons à l’esprit que le bien-être et l’environnement font partie des piliers de l’agilité, je dirais que ça participe à la cohérence fond / forme de cet événement.



## Petite pensée spéciale :

<img src="badge-julie.jpg" class="img-fluid" alt="Badge Agile France 2018 avec les petits pois végétariens ^^"/>

Pour finir Agile France revêt un caractère tout particulier puisque c’est ici que ma collaboration avec Azaé à démarrer. Une conférence sur [_La CNV au service de l’agilité_](http://ajiro.fr/talks/cnv_agile/), que j’ai animé avec Thomas et qui fut le début d’un partenariat qui dure et s’enrichit au fur et à mesure du temps, ainsi que ma première participation à une conférence Agile. Que de chemin parcouru depuis !
Et clin d’oeil particulier, le nouveau venu d’Azaé, Alexis, poursuis ce qui, si on y travaille un peu, pourrait devenir une tradition, puisque cette année, sa participation à Agile France a marqué ses débuts dans le monde de la conférence Agile. Un moment que nous avons eu plaisir à partager !