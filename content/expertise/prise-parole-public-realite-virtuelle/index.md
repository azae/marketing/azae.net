---
title: Expérimenter la prise de parole en public avec la réalité virtuelle
description: "Ambition : Que chacun soit plus serein et plus impactant sur ses prises de parole en public en apprenant les uns des autres."
category: peoples
tags: [methods, beginers]
illustration:
  name: tweet-ppprv.png
presentation: |
  Pourquoi cette formation ?
  Dans le monde d’aujourd’hui nous sommes de plus en plus nombreux à être amenés à prendre la parole en public. Cependant nous avons peu de chances de nous entrainer et la croyance disant que l’aisance innée à la vie dure !
  Aujourd’hui nous vous proposons de casser ce mythe, en mettant l’ensemble des participants au service d’un apprentissage commun. Ce sont donc trois heures pour permettre à chacun de se questionner sur son rapport à la prise de parole en public, de mettre sa connaissance personnelle au service du groupe et d’expérimenter !!!

  Pourquoi ce format ?
  Notre format se veut innovant à plusieurs titre. D'abord nous cassons l'idée reçue selon laquelle il y aurait un sachant détenteur de la bonne réponse, pour mettre en avant le fait que nous apprenons les uns des autres et montrer aux participants qu'ils possèdent déjà de nombreuses ressources en eux.
  Ensuite, nous sommes là en facilitateurs, afin de permettre à toute cette intelligence collective de se mettre en oeuvre dans un climat de confiance. Ainsi notre déroulé propose une démarche progressive pour que chacun puisse expérimenter à son rythme et sous la forme qui lui convient le mieux.
  Enfin nous mettons la technologie au service de l'expérience individuelle avec la réalité virtuelle.

  Cette dernière donne :

    - La possibilité d’être en immersion totale grâce à la réalité virtuelle tout en restant dans un contexte sécurisé et bienveillant : être en situation, tout en sachant que l’on peut interrompre l’expérience à tout moment. Etre dans la « vraie situation » tout en n’étant pas vraiment dans la « vraie vie ».

    - La possibilité de vivre ses émotions en ayant le droit de se tromper ou de recommencer, sans le jugement extérieur et en apprenant « en direct » à accueillir ses émotions

    - Mise en situation concrète sur un public méconnu du participant

    - Une mémorisation plus importante grâce à la visualisation concrète d’un lieu ou d’un contexte de prise de parole, d’un auditoire, d’une situation donnée

    - S'approprier les mécanismes des situations qui déclenchent du stress

  "On ne cherche pas à donner envie aux gens d'être comme nous, mais de leur montrer que ce qui est fantastique c'est d'être eux-même."
objectives: |
  - Initier les participants aux problématiques liées à la prise de parole en public
  - Permettre à chacun d’expérimenter afin d’identifier ses forces et ses principaux axes de travail
  - Mettre en place un cadre sécurisant et ludique qui donne envie à chacun de travailler ses compétences d’orateur
  - Créer un espace d’apprentissage entre pairs afin de mettre en œuvre l’intelligence collective et casser le schéma classique sachant / apprenants
prerequists: |
  - Avoir envie
duration: 1/2 journée
training_plan: |
  - Temps de centrage : point sur son propre vécu en relation avec la prise de parole en public
  - « The Big Idea » : s’exercer à pitcher tout et n’importe quoi, focalisation sur comment dire les choses plus que sur quoi dire
  - Brainstroming prise de parole réussie : échange sur les fondamentaux et activation de l’intelligence collective
  - Mise en pratique avec la réalité virtuelle : la technologie est mise au service de l’entraînement et de l’expérimentation pour que chacun puisse, le temps de quelques minutes, s’entraîner à pitcher en public
resources:
  - name: Présentation Prise de Parole en Public
    url: presentation_prise_parole_public_realite_virtuelle.pdf
aliases:
  - services/training/prise-parole-public-realite-virtuelle.html
  - training/prise-parole-public-realite-virtuelle/
---
