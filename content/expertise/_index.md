---
aliases:
  - /services/expertise/
title: Nos expertises
description: >
    Nous pouvons vous accompagner sur les sujets suivants : Management, Communication NonViolente, Les méthodes agiles (Scrum, Kanban, eXtrem Programming, Lean, ...), Devops, Software caftmanship (TDD, BDD, refactoring, clean code, ...), Technique (Docker, Java, Go, Puppet, Python), L’intelligence collective, Lean startup.
show_on_index:
  order: 4
  icon: fa-rocket
  color: color-sky-blue
  summary: >
    Contactez nous pour que nous puissions définir ensemble le mode d'accompagnement le plus approprié pour vous.
---
