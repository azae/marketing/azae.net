---
title: Les tests unitaires
description: Comment écrire des tests unitaires
nodetail: true
nositemap: true
category: dev
tags: [technical, beginers]
presentation: |
objectives: |
  - Apprendre a écrire des tests unitaires
prerequists: |
  - Être à l'aise avec un langage de programmation pour lequel il existe un système de test unitaires
duration: 2 jours
training_plan: |
  - Définitions et typologie des tests
  - Bonnes pratiques
  - Exercices
resources:
  - name:
    url:
---
