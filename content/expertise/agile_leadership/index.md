---
title: Le leadership agile
description: "Appréhender les clés de la direction agile"
nodetail: true
nositemap: true
category: peoples
tags: [methods]
presentation: |
  Cette formation a pour objectif de travailler sur le leadership de chacun. Que ce soit dans le cadre d'une évolution du rôle de manager vers un rôle de leader, ou une volonté d'augmenter le leadership de chacun au sein d'une équipe, une formation de 3 jours répartie en 5 modules est une réelle opportunité pour chacun d'évoluer dans sa posture.
objectives: |
prerequists: |
duration: 3 jours répartis sur 1 journée et 4 demie-journées, idéalement espacées de 3 semaines
training_plan: |
---
