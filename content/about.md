---
layout: about
title: À Propos
permalink: /about/
description: |
    Passionnés de logiciels libres et convaincus par les méthodes de travail agiles, nous avons décidé de mettre notre passion à votre service. C'est pourquoi Azaé est uniquement composée d'experts qui conjuguent chaque jour passion et professionnalisme. En alliant le plaisir au sérieux nous sommes capables de vous fournir l'expertise adaptée à vos besoins.
---
