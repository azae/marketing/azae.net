# azae.net
Website for Azaé : https://azae.net

# en local

Commencer par metre à jour bootstrap

    git submodule init
    git submodule update

## Avec Docker

Si vous n'avez jamais utilisé la registry gitlab :

    docker login registry.gitlab.com

Puis

    make docker-dev
    make hugo

Le site est alors disponible à l'URL http://localhost:8000/

## Sans Docker

    make hugo

Le site est alors disponible à l'URL http://localhost:8000/

# Taxanomie

vie d'Azae:
  - publication : à propos de nos publications
  - interne : des histoires de la vie dans Azaé

vie d'orateur : Chaque conférence
  - Agile Open France
  - MixIT
  - PyCon
  - Agile tour Lille

vie de coach : En tant que coach agile nous avons différentes activités
  - accompagnement : Accompagenemt des manageurs et des équipes
  - formation : Nous formons des gens
  - méthode : Nos méthodes

outils : Une liste de nos outils
  - speedboat
  - solution focus
  - flowgame

themes : Les thèmes de nos histoires que l'on développe dans nos conférences ... ou pas.
  - zemblanité
  - aide infligée

rituels : Les rituels agiles illustrés par nos histoires.
  - rétrospective
  - embarquement

